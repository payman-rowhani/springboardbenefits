class DataGroup:
  def __init__(self, target_loop_id, target_loop_x12_node, group_items_dict_list):
    self.target_loop_id = target_loop_id
    self.target_loop_x12_node = target_loop_x12_node
    self.group_items_dict_list = group_items_dict_list

  def get_common_data_fields(self, data_fields):
    return list(filter(lambda field: not field.x12_node.is_in_loop(self.target_loop_id), data_fields))

  def get_target_loop_data_fields(self, data_fields):
    return list(filter(lambda field: field.x12_node.is_in_loop(self.target_loop_id), data_fields))

  def get_target_loop_path(self):
    return self.target_loop_x12_node.get_path()