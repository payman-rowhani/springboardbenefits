import tempfile
import xml.etree.ElementTree as ET
from xml.dom.minidom import parseString
from datetime import datetime

from core.x12_node import X12Node
from core.data_group import DataGroup

class InputXmlGenerator:
  """
    This class contains data for generating the xml-representation of x12 edi 834 file.
  """

  def __init__(self, xml_root_template_path, xml_details_template_path, details_data_dict_list, details_data_fields, root_data_fields, output_path=None):
    """
      Creates an instance of InputXmlGenerator.

      Args:
        xml_root_template_path (str): The path to the x12's root (header and footer) xml template.
        xml_details_template_path (str): The path to the x12's details (records) xml template.
        details_data_dict_list (list): The list of dictionaries containing field names and values for all details (records).
        details_data_fields (list): List of DataField elements for details (records) to generate xml from based on details xml template.
        root_data_fields (list): List of DataField elements for root (header and footer) to generate xml from based on root xml template.
        output_path? (str): The optional path to the generated xml (if not passed, a temp file will be generated).

      Returns:
        Instance
    """

    self.xml_root_template_path = xml_root_template_path
    self.xml_details_template_path = xml_details_template_path
    self.details_data_dict_list = details_data_dict_list
    self.details_data_fields = details_data_fields
    self.root_data_fields = root_data_fields
    self.output_path = output_path

    self.root_xml_tree = ET.parse(self.xml_root_template_path)
    self.root_xml_elem = self.root_xml_tree.getroot()
    self.record_xml_template = None

  def generate_xml(self, pretty=False):
    """
      Generates xml and returns the file path to it.

      Args:
        pretty? (bool): The optional flag indicating the generated xml should be formatted and pretty (usefull for debugging).

      Returns:
        The path to the generated xml file (str)
    """

    self._wirte_root()
    self._write_details()
    self._write_transaction_segments_count()

    if not self.output_path:
      file_stream = tempfile.NamedTemporaryFile(mode='w', newline='', encoding='utf-8', suffix='.xml', delete=False)
      output_path = file_stream.name
    else:
      output_path = self.output_path

    if pretty:
      xml_string = ET.tostring(self.root_xml_elem, 'utf-8')
      reparsed = parseString(xml_string)
      reparsed = '\n'.join([line.replace('"', "'").replace('&quot;', '"') if not line.startswith('<?xml') else line for line in reparsed.toprettyxml(indent=' ' * 2, encoding='utf-8').decode().split('\n') if line.strip()])
      with open(output_path, 'wt') as f:
        f.write(reparsed)
        f.write('\n')
    else:
      self.root_xml_tree.write(output_path)

    return output_path

  def _wirte_root(self):
    for data_field in self.root_data_fields:
      field_value = str(data_field.get_formatted_value(None, None)) or ''
      self.root_xml_elem.find(data_field.x12_node.get_path()).text = field_value

    # should be run after all nodes are populated (because of segment positions that may change)
    for data_field in sorted(self.root_data_fields, key=lambda f: f.x12_node.seg_pos or 0, reverse=True):
      data_field.delete_if_needed(self.root_xml_elem, {data_field.field_name: data_field.default_val})

  def _write_details(self):
    details_root_elem = self.root_xml_elem.find(X12Node(loop='DETAIL', parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP')))).get_path())
    for item in self.details_data_dict_list:
      elem = self._get_details_xml_elem_copy()
      generator = _DataFieldXmlGenerator(elem, item, self.details_data_fields)
      generator.write_data_to_xml_elem()
      details_root_elem.append(elem)

  def _write_transaction_segments_count(self):
    transaction_segment_count = len(self.root_xml_elem.find(X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP')).get_path()).findall('.//seg')) - 2
    transaction_counter_element = self.root_xml_elem.find(X12Node(loop='ST_LOOP', seg='SE', ele_index=1, parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))).get_path())
    transaction_counter_element.text = str(transaction_segment_count)

  def _get_details_xml_elem_copy(self):
    if not self.record_xml_template:
      with open(self.xml_details_template_path, 'rt') as f:
        self.record_xml_template = f.read()
    return ET.fromstring(self.record_xml_template)

class _DataFieldXmlGenerator:
  """
    Generates xml for a data field based on a root element and x12 paths specified for each field.
  """

  def __init__(self, xml_element, data_dict, data_fields):
    """
      Creates an instance of _DataFieldXmlGenerator.

      Args:
        xml_element (str): The root xml element to apply changes on.
        data_dict (str): The dictionary of non-formatted field names and values (ex. from csv file).
        data_fields (list): List of DataField elements for each record (details).

      Returns:
        Instance
    """

    self.xml_element = xml_element
    self.data_dict = data_dict
    self.data_fields = data_fields

  def write_data_to_xml_elem(self):
    """
      Writes data to the xml element passed based on x12 paths for each field.

      Args:
        None

      Returns:
        None
    """

    common_data_fields = None
    target_loop_fields = None
    target_loop_index_in_parent = None

    def _set_data_field_value(xml_element, element_path, data_field, data_dict):
      field_value = data_dict.get(data_field.field_name)
      field_value = str(data_field.get_formatted_value(field_value, data_dict)) or ''
      xml_element.find(element_path).text = field_value

    if not isinstance(self.data_dict, DataGroup):
      for data_field in self.data_fields:
        _set_data_field_value(self.xml_element, data_field.x12_node.get_path(), data_field, self.data_dict)
    else:
      common_data_fields = self.data_dict.get_common_data_fields(self.data_fields)
      data_dict = self.data_dict.group_items_dict_list[0]
      for data_field in common_data_fields:
        _set_data_field_value(self.xml_element, data_field.x12_node.get_path(), data_field, data_dict)

      target_loop_fields = self.data_dict.get_target_loop_data_fields(self.data_fields)
      target_loop_index_in_parent = self._get_loop_index_in_parent(self.xml_element, self.data_dict.get_target_loop_path())
      current_loop_index = target_loop_index_in_parent
      for data_dict in self.data_dict.group_items_dict_list:
        target_loop_elem = None
        if current_loop_index != target_loop_index_in_parent:
          target_loop_elem = self._get_loop_copy(self.xml_element, self.data_dict.get_target_loop_path())
        else:
          target_loop_elem = self.xml_element.find(self.data_dict.get_target_loop_path())
        for data_field in target_loop_fields:
          elem_path = data_field.x12_node.get_path().replace(f"loop[@id='{self.data_dict.target_loop_id}']", '').replace('//', '/')
          _set_data_field_value(target_loop_elem, elem_path, data_field, data_dict)
        if current_loop_index != target_loop_index_in_parent:
          self.xml_element.insert(current_loop_index, target_loop_elem)
        current_loop_index += 1

    # should be run after all nodes are populated (because of segment positions that may change)
    for data_field in sorted(self.data_fields, key=lambda f: f.x12_node.seg_pos or 0, reverse=True):
      if not isinstance(self.data_dict, DataGroup):
        data_field.delete_if_needed(self.xml_element, self.data_dict)
      elif data_field in common_data_fields:
        data_field.delete_if_needed(self.xml_element, self.data_dict.group_items_dict_list[0])
      else:
        for (i, data_dict) in enumerate(self.data_dict.group_items_dict_list):
          data_field.delete_if_needed(self.xml_element, data_dict, loop_id_and_index=[self.data_dict.target_loop_id, i+{'2100A': 1, '2300': 2}[self.data_dict.target_loop_id]])

  def _get_loop_copy(self, current_details_element, loop_path):
    return ET.fromstring(ET.tostring(current_details_element.find(loop_path)))

  def _get_loop_index_in_parent(self, current_details_element, loop_path):
    return current_details_element.getchildren().index(current_details_element.find(loop_path))