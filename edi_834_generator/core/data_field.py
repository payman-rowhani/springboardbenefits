class DataField:
  """
    This class contains data for formatting a data field.
  """

  def __init__(self, field_name, x12_node, val_formatter=None, val_len=None, default_val=None, deletable=False, should_delete_func=None):
    """
      Creates an instance of DataField.

      Args:
        field_name (str): The data field name (can be null if a constant is sent in default_val).
        x12_node (X12Node): The node containing the xml path to approriate element where the value should be put.
        val_formatter? (function/dict): The optional value formatter function/dict to format/map field value.
        val_len? (int): The optional value length.
        default_val? (str/function): The optional default value for this field to use when no mapping is found.
        deletable? (bool): If set, the field will be deleted if empty or satisfies the should_delete_func.
        should_delete_func? (function): A function accepting raw_value, formatted value, and data dict list as arguments and returning a boolean indicating whether the field should be deleted or not.

      Returns:
        Instance
    """

    self.field_name = field_name
    self.x12_node = x12_node
    self.val_formatter = val_formatter
    self.val_len = val_len
    self.default_val = default_val
    self.deletable = deletable
    self.should_delete_func = should_delete_func

  def get_formatted_value(self, field_value, data_dict_list):
    """
      Returns the formatted value based on the current value, formatter, length, and default value (str)

      Args:
        field_value (str): The field's raw value for the matched field name.
        data_dict_list (list): The list of dictionaries containing field names and values for all records.

      Returns:
        The formatted value (str)
    """

    default_val = self.default_val
    if default_val and callable(default_val):
      default_val = default_val(data_dict_list)

    default_value = field_value if field_value is not None and field_value != '' else default_val

    if self.val_formatter is not None:
      formatted_value = self._apply_formatter(default_value)
    else:
      formatted_value = default_value

    formatted_value = '' if formatted_value is None else str(formatted_value)
    if self.val_len is not None:
      formatted_value = self._apply_length(formatted_value)

    return formatted_value

  def delete_if_needed(self, xml_element, data_dict_list, loop_id_and_index=None):
    """
      Delete the field from the specified xml element.

      Args:
        xml_element (Element): XML element object.
        data_dict_list (list): The list of dictionaries containing field names and values for all records.
        loop_id_and_index? (list): The loop id and index to search.

      Returns:
        True if deleted or false otherwise (bool)
    """

    raw_value = data_dict_list.get(self.field_name)
    formatted_value = self.get_formatted_value(raw_value, data_dict_list)
    if self.deletable and (not formatted_value or (self.should_delete_func and self.should_delete_func(raw_value, formatted_value, data_dict_list))):
      self.x12_node.delete(xml_element, loop_id_and_index=loop_id_and_index)
      return True
    return False

  def _apply_formatter(self, value):
    if isinstance(self.val_formatter, dict):
      self.val_formatter = {k.lower() if isinstance(k, str) else k: v for k, v in self.val_formatter.items()}
      field_value_as_key = value
      if field_value_as_key is not None and isinstance(field_value_as_key, str):
        field_value_as_key = field_value_as_key.lower()
      return self.val_formatter.get(field_value_as_key, value)
    else:
      formatted_value = self.val_formatter(value)
      return value if formatted_value is None else formatted_value

  def _apply_length(self, value):
    if len(value) < self.val_len:
      return value + (' ' * (self.val_len - len(value)))
    elif len(value) > self.val_len:
      return value[:self.val_len]