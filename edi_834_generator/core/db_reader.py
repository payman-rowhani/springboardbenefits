import csv
import itertools
import pymssql
from datetime import datetime, timedelta

class DbReader:
  """
    This class contains utilities to query SQL Server database.
  """

  def __init__(self, host_or_ip, username, password, db_name, db_port='1433', tds_version=None, connect_timeout=30, now=None):
    """
      Creates an instance of DbReader

      Args:
        host_or_ip (str): The database endpoint address.
        username (str): The database username.
        password (str): The database password.
        db_name (str): The database to load.
        db_port (str): The database port (defaults to 1433).
        tds_version? (str): The optional tds version (required for azure db).
        connect_timeout? (int): The optional database connection timeout in seconds (defaults to 30s).
        now? (datetime): The optional value to represent current time (if not passed, current utc will be used).

      Returns:
        Instance
    """

    self.host_or_ip = host_or_ip
    self.username = username
    self.password = password
    self.db_name = db_name
    self.db_port = db_port
    self.tds_version = tds_version
    self.connect_timeout = connect_timeout
    self.now = now or (datetime.utcnow() - timedelta(hours=4))

    if self.host_or_ip.lower().endswith('windows.net'):
      if self.host_or_ip not in self.username:
        self.username = f'{self.username}@{self.host_or_ip}'
      if not self.tds_version:
        self.tds_version = '7.0'

  def fetch_all_from_query(self, query):
    """
      Args:
        query (str): The query to run.

      Returns:
        The fetched data
    """
    with pymssql.connect(self.host_or_ip, self.username, self.password, self.db_name, port=self.db_port, timeout=self.connect_timeout, tds_version=self.tds_version) as connection:
      with connection.cursor(as_dict=True) as cursor:
        cursor.execute(query)
        return cursor.fetchall()

  def get_data_dict_list(self, query, field_names_map=None):
    """
      Reads the input query into a list of dictionaries representing data in each row
      Args:
        query (str): The query to run.
        field_names_map? (dict): The optional map to replace the field names.
      Returns:
        Data dictionary list (list)
    """

    if field_names_map:
      field_names_map = dict((fieldname.lower(), value.strip()) for (fieldname, value) in field_names_map.items())

    data_dict_list = []
    for row in self.fetch_all_from_query(query):
      row = dict((fieldname.lower(), str(value).strip() if value is not None else '') for (fieldname, value) in row.items() if fieldname)

      if field_names_map:
        for fieldname in field_names_map:
          fieldname = fieldname.lower()
          if fieldname in row:
            new_fieldname = field_names_map[fieldname]
            if fieldname != new_fieldname:
              row[new_fieldname] = row[fieldname]
              del row[fieldname]

      data_dict_list.append(row)

    return data_dict_list