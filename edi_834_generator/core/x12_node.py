class X12Node:
  """
    This class contains data for rendering xml path of an x12 node
  """

  def __init__(self, loop=None, seg=None, seg_pos=None, ele_index=None, subele_index=None, parent=None, use_relative_path=True, related_nodes=None, delete_parent=False):
    """
      Creates an instance of DataField.

      Args:
        loop? (str): The loop id.
        seg? (str): The segment id.
        seg_index? (int): The segment position index (position within the loop).
        ele_index? (int): The element index (the number that comes after the name of parent segment).
        subele_index? (int): The sub-element index (the number that comes after the name of element index with hyphen prefix).
        parent? (X12Node): The current parent node.
        use_relative_path? (bool): If set to True, relative path will be used.
        related_nodes? (list): List of X12Node objects that are related to this node and will be deleted if this node is deleted.
        delete_parent? (bool): If set to True, parent will be deleted when delete operation is called.

      Returns:
        Instance
    """

    self.loop = loop
    self.seg = seg
    self.seg_pos = seg_pos
    self.ele_index = ele_index
    self.subele_index = subele_index
    self.parent = parent
    self.use_relative_path = use_relative_path
    self.related_nodes = related_nodes
    self.delete_parent = delete_parent

  def get_path(self):
    """
      Returns the formatted path to the x12 node in the xml-representation.

      Args:
        None

      Returns:
        The formatted x12 path (str)
    """

    paths = []
    if self.use_relative_path and (not self.parent or not self.parent.get_path().startswith('.')):
      paths.append('.')
    if self.parent:
      paths.append(str(self.parent))
    if self.loop:
      paths.append(f"loop[@id='{self.loop}']")
    if self.seg:
      paths.append(f"seg[@id='{self.seg}']" + (f"[{self.seg_pos}]" if self.seg_pos else ""))
    if self.ele_index:
      if self.subele_index:
        paths.append(f"comp[@id='{self.seg}']")
        paths.append(f"subele[@id='{self.seg or ''}{str(self.ele_index).zfill(2)}-{str(self.subele_index).zfill(2)}']")
      else:
        paths.append(f"ele[@id='{self.seg or ''}{str(self.ele_index).zfill(2)}']")
    return '/'.join(paths)

  def get_ancestor_path(self, ancestor_level=1):
    """
      Returns the formatted path to the x12 node's ancestor in the xml-representation.

      Args:
        ancestor_level (int): Level of ancestor to reach (default is 1 meaning direct parent)

      Returns:
        The parent node's formatted x12 path (str)
    """

    return self.get_path().rsplit('/', ancestor_level)[0] if ancestor_level >= 1 else self.get_path()

  def delete_from_xml(self, xml_element, delete_parent=False, loop_id_and_index=None, fail_silently=True):
    """
      Deletes the node with current path from the specified element,

      Args:
        xml_element (Element): XML element object.
        delete_parent? (bool): Delete parent of the element.
        fail_silently? (bool): Do not throw exception if the current or parent node does not exist.
        loop_id_and_index? (list): The loop id and index to search.

      Returns:
        None
    """

    parent_element_path = self.get_ancestor_path(2 if delete_parent else 1)
    if loop_id_and_index:
      parent_element_path = parent_element_path.replace(f"loop[@id='{loop_id_and_index[0]}']", f"loop[@id='{loop_id_and_index[0]}'][{loop_id_and_index[1]}]")
    parent_element = xml_element.find(parent_element_path) if parent_element_path != '.' else xml_element
    if parent_element is None and fail_silently: return

    current_element_path = self.get_ancestor_path(1 if delete_parent else 0)
    if loop_id_and_index:
      current_element_path = current_element_path.replace(f"loop[@id='{loop_id_and_index[0]}']", f"loop[@id='{loop_id_and_index[0]}'][{loop_id_and_index[1]}]")
    current_element = xml_element.find(current_element_path)
    if current_element is None and fail_silently: return

    parent_element.remove(current_element)

  def delete(self, xml_element, loop_id_and_index=None):
    """
      Deletes the node and related nodes from the specified element based on predefined conditions.

      Args:
        xml_element (Element): XML element object.
        loop_id_and_index? (list): The loop id and index to search.

      Returns:
        None
    """

    self.delete_from_xml(xml_element, self.delete_parent, loop_id_and_index=loop_id_and_index)
    for node in (self.related_nodes or []):
      node.delete(xml_element, loop_id_and_index=loop_id_and_index)

  def is_in_loop(self, loop_id):
    """
      Checks if the node is in the specified loop path.

      Args:
        loop_id (str): Id if the loop to look for.

      Returns:
        Bool
    """

    return f"loop[@id='{loop_id}']" in self.get_path()

  def __str__(self):
    return self.get_path()