import os
import os.path
import importlib
import argparse
import functools
import sys
from datetime import datetime, timedelta

from core.input_xml_generator import InputXmlGenerator
from core.output_x12_generator import OutputX12Generator
from core.aws_utils import AwsUtils

_DB_CREDENTIALS = {
  "db_server": "iris-prod-vpc2.coa9daieb03s.us-east-1.rds.amazonaws.com",
  "db_username": "paymanrowhani",
  "db_password": "F097ef8D38d24af5A603C26072C5A45",
  "db_name": "iris-production",
  "db_port": '1433',
  "db_schema": "dbo"
}

# generate xml from csv
def _generate_input_xml(details_data_dict_list, xml_root_template_path, xml_details_template_path, details_data_fields, root_data_fields, filter_and_map_func, output_path=None, pretty_xml=False, now=None):
  details_data_dict_list = filter_and_map_func(details_data_dict_list)

  input_xml_generator = InputXmlGenerator(xml_root_template_path, xml_details_template_path, details_data_dict_list, details_data_fields, root_data_fields, output_path)
  input_xml_path = input_xml_generator.generate_xml(pretty_xml)
  return input_xml_path

# generate x12 edi 834 from xml
def _generate_output_x12(input_xml_path, output_834_path, repetition_separator='^', compele_separator=':', eol=''):
  output_834_generator = OutputX12Generator(input_xml_path, output_834_path)
  output_834_generator.generate_834(repetition_separator=repetition_separator, compele_separator=compele_separator, eol=eol)
  return output_834_path

# put file on aws s3
def _put_file_to_s3(local_file_path, bucket_name, bucket_file_key, aws_region_name=None, aws_access_key=None, aws_secret_key=None):
  AwsUtils(aws_region_name, aws_access_key, aws_secret_key).upload_file_to_s3(bucket_name, local_file_path, bucket_file_key)

# get vendor class from its name and context
def get_vendor_class(vendor, now=None, db_context=None, data_folder_path=None, file_name_prefix=None, delivery_name=None, is_production=False, output_edi_path=None, delivery_directory=None, bucket_name=None):
  vendor_module = importlib.import_module(f'vendors.vendor_{vendor.lower()}')
  vendor_cls = getattr(vendor_module, f'VENDOR_{vendor.upper()}')(
    now=now,
    db_context=db_context,
    data_folder_path=data_folder_path,
    file_name_prefix=file_name_prefix,
    delivery_name=delivery_name,
    is_production=is_production,
    output_edi_path=output_edi_path,
    delivery_directory=delivery_directory,
    bucket_name=bucket_name
  )
  return vendor_cls

# get available vendor names based on script files in "vendors" directory
def get_vendor_names():
  vendors = []
  for file in os.listdir('vendors'):
    if '.py' in file and '__init__' not in file:
      vendors.append(os.path.splitext(os.path.basename(file))[0].upper().replace('VENDOR_', ''))
  return vendors

# get command line args based on converter type
def get_command_line_args(converter_type):
  converter_type = converter_type.lower() # can be csv/db
  vendors = get_vendor_names()
  parser = argparse.ArgumentParser(description=f'Converts {"CSV" if converter_type == "csv" else "DB"} to X12 834 EDI.')

  parser.add_argument('--vendor', dest='vendor', required=True, choices=vendors, help=f'Receiver-sender combination code, such as UHC_834_ASMO (same as Talend job names)')
  parser.add_argument('--data-folder-path', dest='data_folder_path', required=False, help='Path to data folder for local files (if not specified, default value for the vendor will be used)')
  parser.add_argument('--file-name-prefix', dest='file_name_prefix', required=False, help='File name prefix for local files (if not specified, default value for the vendor will be used)')
  parser.add_argument('--delivery-name', dest='delivery_name', required=False, help='Delivery name to put on bucket (if not specified, default value for the vendor will be used)')
  parser.add_argument('--is-production', dest='is_production', action='store_true', help='Flag for production environment (if not specified, default value for the vendor will be used)')
  parser.add_argument('--output-edi-path', dest='output_edi_path', required=False, help='Path to the output x12 834 edi file (if not specified, name is inferred from overriden or default data-folder-path and file-name-prefix)')
  parser.add_argument('--delivery-directory', dest='delivery_directory', required=False, help='Delivery directory to put on bucket (if not specified, default value for the vendor will be used)')
  parser.add_argument('--bucket-name', dest='bucket_name', required=False, help='Bucket name (if not specified, default value for the vendor will be used; use " " if you don\'t want to upload the converted file to any bucket)')
  parser.add_argument('--xml-output', dest='intermediate_xml_output_path', required=False, help='Path to output intermedite xml file (if not provided, it will be deleted)')
  parser.add_argument('--aws-region', dest='aws_region_name', required=False, help='AWS region name (if not provided, default value from profile will be used)')
  parser.add_argument('--aws-access-key', dest='aws_access_key', required=False, help='AWS access key (if not provided, default value from profile will be used)')
  parser.add_argument('--aws-secret-key', dest='aws_secret_key', required=False, help='AWS secret key (if not provided, default value from profile will be used)')

  if converter_type == "csv":
    parser.add_argument('csv_file_path', nargs=1, help='Path to input CSV file')
    parser.add_argument('--csv-delimiter', dest='csv_delimiter', required=False, help='CSV delimiter (defaults to comma)', default=',')
    parser.add_argument('--csv-quote-char', dest='csv_quote_char', required=False, help='CSV quote character (defaults to null; double quotes are automatically detected)', default=None)
  else:
    parser.add_argument('--db-server', dest='db_server', required=False, help='Database server (if not provided, default val will be used)', default=_DB_CREDENTIALS['db_server'])
    parser.add_argument('--db-username', dest='db_username', required=False, help='Database username (if not provided, default val will be used)', default=_DB_CREDENTIALS['db_username'])
    parser.add_argument('--db-password', dest='db_password', required=False, help='Database password (if not provided, default val will be used)', default=_DB_CREDENTIALS['db_password'])
    parser.add_argument('--db-name', dest='db_name', required=False, help='Database name (if not provided, default val will be used)', default=_DB_CREDENTIALS['db_name'])
    parser.add_argument('--db-port', dest='db_port', type=int, required=False, help='Database port (if not provided, default val will be used)', default=_DB_CREDENTIALS['db_port'])
    parser.add_argument('--db-context', dest='db_context', nargs="*", required=False, action="append", help=f'Database context variabels in key=val format (if not provided, default values will be used)')

  try:
    args = parser.parse_args()
    args.pretty_intermediate_xml=bool(args.intermediate_xml_output_path)
    args.keep_intermediate_xml=bool(args.intermediate_xml_output_path)
    args.now = datetime.utcnow() # - timedelta(hours=4) EST

    if converter_type == "csv":
      args.csv_file_path = args.csv_file_path[0]
    else:
      db_context = functools.reduce(lambda x, y: x + y, args.db_context or [[]])
      db_context = list(filter(lambda x: '=' in x, db_context))
      args.db_context = dict([x.split('=') for x in db_context])

    return args.__dict__
  except:
    sys.exit(0)

# main function for conversion of data list to 834
def convert_data_list_to_834(details_data_dict_list, vendor, db_context=None, data_folder_path=None, file_name_prefix=None, delivery_name=None, is_production=False, output_edi_path=None, delivery_directory=None, bucket_name=None, intermediate_xml_output_path=None, pretty_intermediate_xml=False, keep_intermediate_xml=False, now=None, verbose=True, create_xml_only=False, aws_region_name=None, aws_access_key=None, aws_secret_key=None):
  """
    Top-level function to convert CSV/DB into x12 834 edi file.

    Args:
      details_data_dict_list (list): List of DataField elements for details (records) to generate xml from based on details xml template.

    Returns:
      The path to x12 834 edi file or the intermediate xml representation (str).
  """

  if not now:
    now = datetime.utcnow() # - timedelta(hours=4) # EST

  # get vendor class from its name
  vendor_cls = get_vendor_class(
    vendor=vendor,
    now=now,
    db_context=db_context,
    data_folder_path=data_folder_path,
    file_name_prefix=file_name_prefix,
    delivery_name=delivery_name,
    is_production=is_production,
    output_edi_path=output_edi_path,
    delivery_directory=delivery_directory,
    bucket_name=bucket_name
  )

  # generate input xml representation of edi 834
  if verbose: print('Generating the temporary intermediate XML file...')
  input_xml_path = _generate_input_xml(
    details_data_dict_list=details_data_dict_list,
    xml_root_template_path=vendor_cls.get_root_template_path(),
    xml_details_template_path=vendor_cls.get_details_template_path(),
    details_data_fields=vendor_cls.get_details_data_fields(convert_to_list=True),
    root_data_fields=vendor_cls.get_root_data_fields(convert_to_list=True),
    filter_and_map_func=vendor_cls.filter_and_map,
    pretty_xml=pretty_intermediate_xml,
    output_path=intermediate_xml_output_path,
    now=now
  )
  if verbose and (keep_intermediate_xml or create_xml_only): print(f'XML file saved in {input_xml_path}.')

  # return xml path if no 834 conversion is needed
  if create_xml_only:
    return input_xml_path

  # generate output 834 from the xml representation
  if verbose: print('Converting XML to X12 EDI 834...')
  vendor_root_info_map = vendor_cls.get_root_info_map()
  output_834_path = _generate_output_x12(
    input_xml_path=input_xml_path,
    output_834_path=vendor_cls.get_edi_file_path(),
    repetition_separator=vendor_root_info_map['repetition_separator'],
    compele_separator=vendor_root_info_map['compele_separator'],
    eol=vendor_root_info_map['eol']
  )
  if verbose: print(f'Output EDI 834 file saved in {output_834_path}.')

  # put converted file on s3 bucket if needed
  if vendor_cls.should_put_file_on_bucket():
    if verbose: print('Uploading output EDI 834 file to s3 bucket...')
    bucket_name, bucket_file_key = vendor_cls.get_delivery_bucket_and_file()
    _put_file_to_s3(
      local_file_path=vendor_cls.get_edi_file_path(),
      bucket_name=bucket_name,
      bucket_file_key=bucket_file_key,
      aws_region_name=aws_region_name,
      aws_access_key=aws_access_key,
      aws_secret_key=aws_secret_key
    )
    if verbose: print(f'Output EDI 834 uploaded to s3 bucket {bucket_name} in path {bucket_file_key}.')

  # remove intermediate xml if not needed
  if not keep_intermediate_xml:
    os.remove(input_xml_path)

  # return 834 path
  return output_834_path