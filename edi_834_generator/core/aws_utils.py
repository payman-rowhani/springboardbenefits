import os
import boto3

class AwsUtils:
  """
    This class contains utilities for aws cli resources
  """

  def __init__(self, aws_region_name=None, aws_access_key=None, aws_secret_key=None):
    """
      Args:
        aws_region_name? (str): Optional AWS region name.
        aws_access_key? (str): Optional AWS access key.
        aws_secret_key? (str): Optional AWS secret key.

      Returns:
        Instance
    """

    default_credentials = boto3.Session().get_credentials()
    self.aws_region_name = aws_region_name
    self.aws_access_key = aws_access_key or default_credentials.access_key
    self.aws_secret_key = aws_secret_key or default_credentials.secret_key

  def __get_aws_client(self, resource_name):
    return boto3.client(resource_name,
                        region_name=self.aws_region_name,
                        aws_access_key_id=self.aws_access_key,
                        aws_secret_access_key=self.aws_secret_key)

  def get_s3_bucket_client(self):
    """
      Returns:
        AWS S3 Bucket client
    """

    return self.__get_aws_client('s3')

  def upload_file_to_s3(self, bucket_name, file_path_to_upload, file_key_on_bucket=None, use_server_side_encryption=True):
    """
      Args:
        bucket_name: The bucket name to upload to.
        file_path_to_upload (str): The path to the file to upload.
        file_key_on_bucket? (str): The name of the key to upload to on bucket.
        use_server_side_encryption? (bool): If set, server side encryption is used

      Returns:
        None
    """

    if not file_key_on_bucket:
      file_key_on_bucket = os.path.basename(file_path_to_upload)

    s3_client = self.get_s3_bucket_client()
    s3_client.upload_file(file_path_to_upload, bucket_name, file_key_on_bucket, ExtraArgs={'ServerSideEncryption': 'AES256'} if use_server_side_encryption else {})