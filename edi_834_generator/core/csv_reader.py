import csv
import itertools
from datetime import datetime, timedelta

class CsvReader:
  """
    This class contains utilities to parse a CSV file.
  """

  def __init__(self, csv_file_path, csv_delimiter=',', csv_quote_char=None, now=None):
    """
      Creates an instance of CsvReader

      Args:
        csv_file_path (str): The path to the input csv file.
        csv_delimiter? (str): The optional csv delimiter (defaults to comma).
        csv_quote_char? (str): The optional csv quote character (defaults to null).
        now? (datetime): The optional value to represent current time (if not passed, current utc will be used).

      Returns:
        Instance
    """

    self.csv_file_path = csv_file_path
    self.csv_delimiter = csv_delimiter
    self.csv_quote_char = csv_quote_char
    self.now = now or datetime.utcnow() # - timedelta(hours=4) EST

  def get_data_dict_list(self, field_names=None, field_names_map=None, max_rows_count=None, row_offset=0):
    """
      Parses the input csv file into a list of dictionaries representing data in each row
      Args:
        field_names? (list): The optional list of field names to be used as dictionary keys (if null, the first row is used).
        field_names_map? (dict): The optional map to replace the field names.
        max_rows_count? (int): The optional maximum number of rows to return (defaults to all rows).
        row_offset? (int): The optional number of rows to skip (defaults to 0).
      Returns:
        Data dictionary list (list)
    """

    if field_names_map:
      field_names_map = dict((fieldname.lower(), value.strip()) for (fieldname, value) in field_names_map.items())

    data_dict_list = None
    with open(self.csv_file_path) as csv_file:
      quote_char = self.csv_quote_char
      if not quote_char:
        quote_char = '"' if len(csv_file.readline() and csv_file.readline().split('"')) >= 3 else None
        csv_file.seek(0, 0)

      reader = csv.DictReader(csv_file, fieldnames=field_names, delimiter=self.csv_delimiter)
      if quote_char:
        reader.quotechar = quote_char

      data_dict_list = []
      for row in itertools.islice(reader, row_offset, (row_offset + max_rows_count) if max_rows_count else None):
        row = dict((fieldname.lower(), value.strip() if value else '') for (fieldname, value) in row.items() if fieldname)

        if field_names_map:
          for fieldname in field_names_map:
            fieldname = fieldname.lower()
            if fieldname in row:
              new_fieldname = field_names_map[fieldname]
              if fieldname != new_fieldname:
                row[new_fieldname] = row[fieldname]
                del row[fieldname]

        data_dict_list.append(row)

    return data_dict_list