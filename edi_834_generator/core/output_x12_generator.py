import tempfile
import os
import os.path
import xml.etree.cElementTree as CET

import pyx12.x12file
import pyx12.xmlx12_simple

class OutputX12Generator:
  """
    This class contains data for generating x12 edi 834 file.
  """

  def __init__(self, input_xml_path, output_834_path):
    """
      Creates an instance of OutputX12Generator.

      Args:
        input_xml_path (str): The path to xml-representation of x12 edi 834 file.
        output_834_path (str): The path to generate the output x12 834 edi file.

      Returns:
        Instance
    """

    self.input_xml_path = input_xml_path
    self.output_834_path = output_834_path

  def generate_834(self, repetition_separator='^', seg_terminator='~', ele_delimiter='*', compele_separator=':', eol=''):
    """
      Generates x12 edi 834 file by converting the xml-representation to edi file.

      Args:
        repetition_separator? (str): Repetition separator.
        seg_terminator? (str): Segment terminator.
        ele_delimiter? (str): Element delimiter.
        compele_separator? (str): Component/sub element separator.
        eol? (str): End of line character.

      Returns:
        The path to the generated x12 edi 834 file (str)
    """

    if not self.output_834_path:
      file_stream = tempfile.NamedTemporaryFile(mode='w', newline='', encoding='utf-8', suffix='.834', delete=False)
      self.output_834_path = file_stream.name

    output_directory = os.path.dirname(self.output_834_path)
    if output_directory and not os.path.exists(output_directory):
      os.makedirs(output_directory)

    with open(self.input_xml_path) as xml_source:
      with open(self.output_834_path, 'wt') as x12_dest:
        # pyx12.xmlx12_simple.convert(xml_source, x12_dest) # commented since we need customized args
        wr = pyx12.x12file.X12Writer(x12_dest, seg_term=seg_terminator, ele_term=ele_delimiter, subele_term=compele_separator, eol=eol, repetition_term=repetition_separator)
        doc = CET.parse(xml_source)
        for node in doc.iter():
          if node.tag == 'seg':
            wr.Write(pyx12.xmlx12_simple.get_segment(node))
        wr.Close()

    return self.output_834_path