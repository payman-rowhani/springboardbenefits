import vendors.base.common as common
import vendors.base.vendor_cigna_base as base

class VENDOR_CIGNA_MULTICOVERAGE_834_HFHI(base.VendorCignaBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='91-1914868',
      sender_tax_id='911914868',
      sender_name='Habitat for Humanity'
    )

  def child_class_filter_and_map(self, records):
    for record in records:
      record['state_code']= '1' if record['homeaddress_state'] in ['NC', 'CA'] else '2' if record['homeaddress_state'] == 'TX' else '0'
      record['family_code'] = '1' if common.CommonUtils.to_boolean(record['spousecoverageelected']) or common.CommonUtils.to_boolean(record['childcoverageelected']) else '0'

    variant_translation = [
      {'pvid': '731', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '732', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '733', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '734', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '735', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '736', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '728', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '729', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '730', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '731', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '732', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '733', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '734', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '735', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '736', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '728', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '729', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '730', 'statecode': '0', 'familycode': '1', 'i': '1'},
      {'pvid': '731', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '732', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '733', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '734', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '735', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '736', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '728', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '729', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '730', 'statecode': '2', 'familycode': '0', 'i': '0'},
      {'pvid': '731', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '732', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '733', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '734', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '735', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '736', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '728', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '729', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '730', 'statecode': '2', 'familycode': '1', 'i': '1'},
      {'pvid': '731', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '732', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '733', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '734', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '735', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '736', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '728', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '729', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '730', 'statecode': '1', 'familycode': '0', 'i': '2'},
      {'pvid': '731', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '732', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '733', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '734', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '735', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '736', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '728', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '729', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '730', 'statecode': '1', 'familycode': '1', 'i': '3'},
      {'pvid': '726', 'statecode': '2', 'familycode': '0', 'i': '3'},
      {'pvid': '726', 'statecode': '2', 'familycode': '1', 'i': '3'},
      {'pvid': '727', 'statecode': '2', 'familycode': '0', 'i': '3'},
      {'pvid': '727', 'statecode': '2', 'familycode': '1', 'i': '3'},
      {'pvid': '726', 'statecode': '0', 'familycode': '1', 'i': '0'},
      {'pvid': '726', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '727', 'statecode': '0', 'familycode': '1', 'i': '0'},
      {'pvid': '727', 'statecode': '0', 'familycode': '0', 'i': '0'},
      {'pvid': '726', 'statecode': '1', 'familycode': '1', 'i': '0'},
      {'pvid': '726', 'statecode': '1', 'familycode': '0', 'i': '0'},
      {'pvid': '727', 'statecode': '1', 'familycode': '1', 'i': '0'},
      {'pvid': '727', 'statecode': '1', 'familycode': '0', 'i': '0'}
    ]
    records = common.CommonUtils.join_datasets_first_match(records, variant_translation, ['planvariant_id', 'state_code', 'family_code'], ['pvid', 'statecode', 'familycode'])

    benefit_type_translation = [
      {'benefittype': '1', 'codeoverride': ''},
      {'benefittype': '1', 'codeoverride': '3337327HABAC VISN'},
    ]
    records = common.CommonUtils.join_datasets_all_matches(records, benefit_type_translation, 'benefit_type', 'benefittype', 'left')

    for record in records:
      carrier_plan = record['carrierplandata4']
      _merged_plans = (record['carrierplandata1'] or '') + (record['carrierplandata2'] or '') + ' ' + (record['carrierplandata3'] or '').split('|')[int(record['i'])]
      carrier_plans = record['codeoverride'] or _merged_plans
      self._apply_general_mapping(record, carrier_plan, carrier_plans)
      if record['codeoverride']: record['benefit_type'] = '3'

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 145, "carrierId": 2, "enrollmentPeriodId": 62, "DaysInPast": 7},
      "data_folder_path": "C:\\TalendDataSpace\\CignaEDI\\HFHI",
      "file_name_prefix": "Cigna_HFHI_Core_Data",
      "delivery_name": "XO16000__xo10001i.60261.date.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([1, 2 ,3]) + \
            common.QueryUtils.get_ordeby_ssn_statement(include_last_name=True)
    return query.format(**self.db_context) if not raw else query