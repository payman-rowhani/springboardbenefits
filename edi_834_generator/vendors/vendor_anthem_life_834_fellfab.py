import vendors.base.common as common
import vendors.base.vendor_anthem_base as base

class VENDOR_ANTHEM_LIFE_834_FELLFAB(base.VendorAnthemBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='GA55815E'
    )

  def child_class_filter_and_map(self, records):
    benefit_type_translation = [
      {'benefittype': '21', 'plancode': 'GA2560B001', 'newbenefittype': '21', 'division': '85'},
      {'benefittype': '21', 'plancode': 'GA2560C001', 'newbenefittype': '22', 'division': '85'},
      {'benefittype': '21', 'plancode': 'GA2560B002', 'newbenefittype': '21', 'division': '87'},
      {'benefittype': '21', 'plancode': 'GA2560C002', 'newbenefittype': '22', 'division': '87'},
      {'benefittype': '21', 'plancode': 'GA2560C003', 'newbenefittype': '22', 'division': '86'},
    ]
    records = common.CommonUtils.join_datasets_all_matches(records, benefit_type_translation, ['benefit_type', 'division_id'], ['benefittype', 'division'], 'left')

    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      basic_plan_code = record['plancode']
      self._apply_general_mapping(record, basic_plan_code)
      record['benefit_type'] = record['newbenefittype']
      common.MapUtils.change_hire_code_and_value(record, '356', record['benefiteffectivedate'])

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 26, "carrierId": 25, "enrollmentPeriodId": 76, "DaysInPast": 8},
      "data_folder_path": "C:\\TalendDataSpace\\AnthemEDI\\Fellfab",
      "file_name_prefix": "Anthem_Fellfab_Life_Data",
      "delivery_name": "GA55815E.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([21]) + \
            common.QueryUtils.get_ordeby_ssn_statement_only() + \
            common.QueryUtils.get_ordeby_name_statement()
    return query.format(**self.db_context) if not raw else query