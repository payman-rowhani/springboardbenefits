import vendors.base.common as common
import vendors.base.vendor_cigna_base as base

class VENDOR_CIGNA_MULTICOVERAGE_834_COLTENE(base.VendorCignaBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='133568101',
      sender_tax_id='133568101',
      sender_name='Coltene'
    )

  def child_class_filter_and_map(self, records):
    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      carrier_plan = record['carrierplandata4']
      carrier_plans = (record['carrierplandata1'] or '') + (record['carrierplandata2'] or '') + (record['carrierplandata3'] or '')
      self._apply_general_mapping(record, carrier_plan, carrier_plans)

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 150, "carrierId": 2, "enrollmentPeriodId": 72, "DaysInPast": 8},
      "data_folder_path": "C:\\TalendDataSpace\\CignaEDI\\Coltene",
      "file_name_prefix": "Cigna_Coltene_Core_Data",
      "delivery_name": "XO16000__xo10001i.60689.date.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([2 ,3]) + \
            common.QueryUtils.get_ordeby_ssn_statement(include_last_name=True)
    return query.format(**self.db_context) if not raw else query