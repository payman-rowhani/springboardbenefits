import vendors.base.common as common
import vendors.base.vendor_base as base

# EDI Map: UHCXMLto834.xml
class VENDOR_UCH_834_ASMO(base.VendorBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='621348671',
      sender_name='ASMO',
      sender_tax_id='621348671',
      receiver_id='411289245',
      receiver_name='UNITEDHEALTH GROUP',
      receiver_code='411289245',
      ts_ref_number='ASMO',
      master_policy_id='ASMONCF',
      application_sender_code='Springboard',
      application_receiver_code='CES',
      entity_identifier_code='TV',
      repetition_separator='!',
      usage_indicator='P',
      first_interchange_id_qualifier='30',
      second_interchange_id_qualifier='30'
    )

  @common.GroupUtils.group_by_member_elections
  def filter_and_map(self, records):
    records = self._apply_general_filtering(records)

    for record in records:
      self._apply_general_mapping(record)
      common.MapUtils.set_earning_value(record)
      common.MapUtils.set_contact_codes_and_info(record, include_home_phone=False, include_work_phone=False, include_email=True)
      common.MapUtils.set_custom_employee_info(record, 'ABB', record['employee_ssn'])
      common.MapUtils.set_basic_plan_code(record, record['carrierplandata4'])
      common.MapUtils.remove_employment_end_date(record)
      common.MapUtils.set_employment_status_code(record, 'AC')
      common.MapUtils.set_medicare_plan_code(record)
      common.MapUtils.set_coverage_code(record)
      carrier_plan = (record['carrierplandata5'] or '').split('|')[0 if record['homeaddress_state'] == 'MI' else -1]
      common.MapUtils.set_plan_code(record, carrier_plan)
    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 6, "carrierId": 9, "enrollmentPeriodId": 64, "DaysInPast": 8},
      "data_folder_path": "C:\\TalendDataSpace\\UHCEDI\\UHC_834_ASMO",
      "file_name_prefix": "UHC_ASMO_Data",
      "delivery_name": "asmo_uhc_springboarddate.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_no_condition() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions()
    return query.format(**self.db_context) if not raw else query