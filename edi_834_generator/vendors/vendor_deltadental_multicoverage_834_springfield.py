import vendors.base.vendor_base as base
import vendors.base.common as common

# EDI Map: DDXMLto834.xml
class VENDOR_DELTADENTAL_MULTICOVERAGE_834_SPRINGFIELD(base.VendorBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='560894904',
      sender_name='Springfield Clinic',
      sender_tax_id='370701328',
      receiver_id='362612058',
      receiver_name='DELTA DENTAL OF ILLINOIS',
      receiver_code='362612058',
      ts_ref_number='S20170124141107',
      master_policy_id='0101067',
      application_sender_code='560894904',
      application_receiver_code='DDIL',
      entity_identifier_code='BO',
      repetition_separator='|',
      usage_indicator='P',
      first_interchange_id_qualifier='30',
      second_interchange_id_qualifier='30'
    )

  def filter_and_map(self, records):
    records = self._apply_general_filtering(records)
    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      super()._apply_general_mapping(record)
      common.MapUtils.set_employment_status_code(record, 'PT')
      common.MapUtils.remove_employee_number(record)
      common.MapUtils.remove_employment_end_date(record)
      common.MapUtils.set_contact_codes_and_info(record, include_home_phone=False, include_work_phone=False, include_email=True)
      common.MapUtils.set_coverage_code(record, by_dependent_count=True)
      plan_code = '-'.join([record['carrierplandata1'] or '', record['carrierplandata2'] or '', record['carrierplandata3'] or '', record['carrierplandata4'] or ''])
      common.MapUtils.set_basic_plan_code(record, plan_code)
      common.MapUtils.set_plan_code(record, 'MEC')

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 126, "carrierId": 37, "enrollmentPeriodId": 48, "DaysInPast": 14},
      "data_folder_path": "C:\\TalendDataSpace\\DeltaDentalEDI\\Springfield",
      "file_name_prefix": "DD_Springfield_Data",
      "delivery_name": "XO16000__xo10001i.57484.date.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_dependent_count() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_orderby_name_and_relationship()
    return query.format(**self.db_context) if not raw else query