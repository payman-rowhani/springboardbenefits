import vendors.base.common as common
import vendors.base.vendor_anthem_base as base

# EDI MAP: BCBSNCXMLto834
class VENDOR_ANTHEM_MULTICOVERAGE_834_ASMO(base.VendorAnthemBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='560894904',
      sender_name='ASMO',
      sender_tax_id='621348671',
      receiver_id='621348671',
      receiver_name='BCBSNC',
      receiver_code='560894904',
      application_sender_code='560894904',
      application_receiver_code='621348671',
      usage_indicator='P',
      first_interchange_id_qualifier='30',
      second_interchange_id_qualifier='30',
    )

  def child_class_filter_and_map(self, records):
    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      basic_plan_code = record['carrierplandata1']
      self._apply_general_mapping(record, basic_plan_code, set_custom_info=False)
      common.MapUtils.set_coverage_code(record)
      carrier_plan = record['carrierplandata3'] if common.CommonUtils.to_boolean(record['spousecoverageelected']) or common.CommonUtils.to_boolean(record['childcoverageelected']) else record['carrierplandata2']
      common.MapUtils.set_plan_code(record, carrier_plan)

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 6, "carrierId": 5, "enrollmentPeriodId": 64, "DaysInPast": 8},
      "data_folder_path": "C:\\TalendDataSpace\\BSBCNCEDI\\ASMO",
      "file_name_prefix": "BCBSNC_ASMO_Core_Data",
      "delivery_name": "OE_ASMOManufacturing_BCBSNC_date.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([1]) + \
            common.QueryUtils.get_division_id_condition(1) + \
            common.QueryUtils.get_ordeby_name_statement()
    return query.format(**self.db_context) if not raw else query