import vendors.base.common as common
import vendors.base.vendor_cigna_base as base

# EDI Map: CignaXMLto834ASMO.xml
class VENDOR_CIGNA_MULTICOVERAGE_834_ASMO(base.VendorCignaBase):
  def get_root_info_map(self, **kwargs):
    info = super().get_root_info_map(
      sender_id='047790171',
      sender_tax_id='621348671',
      sender_name='ASMO',
      master_policy_id='0101067',
      first_interchange_id_qualifier='01',
      ts_ref_number='0001',
      entity_identifier_code='TV',
      action_code='RX'
    )
    if kwargs: info.update(kwargs)
    return info

  def child_class_filter_and_map(self, records):
    for record in records:
      record['carrierplandata2'] = (record['carrierplandata2'] or '') + 'A  '

    carrier_plan_translations = [
      {'state': 'TX', 'cpd2': 'AMIA  ', 'overridecode': '3340062AMIA  DENTH'},
      {'state': 'MI', 'cpd2': 'AMIA  ', 'overridecode': '3340062AMIA  DPPOH'},
    ]
    records = common.CommonUtils.join_datasets_first_match(records, carrier_plan_translations, ['homeaddress_state', 'carrierplandata2'], ['state', 'cpd2'], 'left')

    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      carrier_plan = record['carrierplandata4']
      carrier_plans = record['overridecode'] or ((record['carrierplandata1'] or '') + (record['carrierplandata2'] or '') + (record['carrierplandata3'] or ''))
      self._apply_general_mapping(record, carrier_plan, carrier_plans, leave_dependent_ssn_unchanged=True, keep_employment_end_date=True)
      common.MapUtils.set_earning_value(record)
      common.MapUtils.set_contact_codes_and_info(record, include_home_phone=False, include_work_phone=False, include_email=True)
      common.MapUtils.set_health_related_code(record)
      common.MapUtils.set_late_enrollment_indicator(record)
      common.MapUtils.set_medicare_plan_code(record)
      common.MapUtils.set_employment_status_code(record, 'AC')
      if record['statuschangeeffectivedate'] == record['dateofhire'] or (not record['dependentcanceled'] and not record['canceled']): common.MapUtils.remove_employment_end_date(record)

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 6, "carrierId": 2, "enrollmentPeriodId": 64, "DaysInPast": 8},
      "data_folder_path": "C:\\TalendDataSpace\\CignaEDI\\ASMO\\",
      "file_name_prefix": "Cigna_ASMO_Data",
      "delivery_name": "XO16000__xo10001i.48261.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([1, 2 ,3]) + \
            common.QueryUtils.get_orderby_name_and_relationship_and_dob()
    return query.format(**self.db_context) if not raw else query