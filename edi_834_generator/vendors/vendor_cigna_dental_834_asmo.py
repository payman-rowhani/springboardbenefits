import vendors.base.common as common
import vendors.vendor_cigna_multicoverage_834_asmo as base

# EDI Map: Cigna_Dental_834_ASMO.xml
class VENDOR_CIGNA_DENTAL_834_ASMO(base.VENDOR_CIGNA_MULTICOVERAGE_834_ASMO):
  def get_root_info_map(self):
    return super().get_root_info_map(
      usage_indicator='T'
    )

  def child_class_filter_and_map(self, records):
    for record in records:
      carrier_plan = None
      _merged_plans = (record['carrierplandata1'] or '') + (record['carrierplandata2'] or '') + 'A  ' + (record['carrierplandata3'] or '')
      carrier_plans = '3340062AMIA  DENTH' if record['homeaddress_state'] == 'TX' and record['carrierplandata2'] == 'AMI' else '3340062AMIA  DPPOH' if record['homeaddress_state'] == 'MI' and record['carrierplandata2'] == 'AMI' else _merged_plans
      self._apply_general_mapping(record, carrier_plan, carrier_plans, leave_dependent_ssn_unchanged=True, keep_employment_end_date=True, use_alt_cancel_time=False)
      common.MapUtils.set_employment_status_code(record, 'FT' if record['isfulltime'] == '1' else 'AC')
      common.MapUtils.set_earning_value(record)
      common.MapUtils.set_contact_codes_and_info(record, include_home_phone=False, include_work_phone=False, include_email=True)
      common.MapUtils.set_health_related_code(record)
      common.MapUtils.set_late_enrollment_indicator(record)
      common.MapUtils.set_medicare_plan_code(record)
      if record['status'] != '5': common.MapUtils.remove_employment_end_date(record)
      if record['statuschangeeffectivedate'] and record['status'] == '5': common.MapUtils.remove_date_of_hire(record)

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 6, "carrierId": 2, "enrollmentPeriodId": 64, "DaysInPast": 7},
      "data_folder_path": "C:\\TalendDataSpace\\CignaEDI\\Cigna_834_ASMO",
      "file_name_prefix": "Cigna_Dental_ASMO_Data",
      "delivery_name": None, # don't put on s3
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_no_condition() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions(remove_dependent=True)
    return query.format(**self.db_context) if not raw else query