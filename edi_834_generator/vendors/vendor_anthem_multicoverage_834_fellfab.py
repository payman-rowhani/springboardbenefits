import vendors.base.common as common
import vendors.base.vendor_anthem_base as base

class VENDOR_ANTHEM_MULTICOVERAGE_834_FELLFAB(base.VendorAnthemBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='GA55801E'
    )

  def child_class_filter_and_map(self, records):
    division_translation = [
      {'divisionid': '85', 'i': '0'},
      {'divisionid': '87', 'i': '1'}
    ]
    records = common.CommonUtils.join_datasets_first_match(records, division_translation, 'division_id', 'divisionid', 'left')

    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      basic_plan_code = (record['carrierplandata1'] or '').split('|')[2 if not record['i'] else int(record['i'])]
      self._apply_general_mapping(record, basic_plan_code)
      common.MapUtils.change_hire_code_and_value(record, '356', record['enrollmentdatetime'])

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 26, "carrierId": 25, "enrollmentPeriodId": 76, "DaysInPast": 7},
      "data_folder_path": "C:\\TalendDataSpace\\AnthemEDI\\Fellfab",
      "file_name_prefix": "Anthem_Fellfab_Core_Data",
      "delivery_name": "GA55801E.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([1, 2 ,3]) + \
            common.QueryUtils.get_ordeby_name_statement()
    return query.format(**self.db_context) if not raw else query