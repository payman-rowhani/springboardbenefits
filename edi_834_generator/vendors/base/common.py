from dateutil.parser import parse as dateparse
from datetime import datetime, timedelta
import pandas as pd
import os

from core.x12_node import X12Node
from core.data_group import DataGroup

#####################
#   Common Utils    #
#####################
class CommonUtils:
  @staticmethod
  def to_boolean(val):
    return str(val or '').lower() in ['1', 'true']

  @staticmethod
  def to_binary(val):
    return '1' if CommonUtils.to_boolean(val) else '0'

  @staticmethod
  def join_datasets_unique_match(left, right, left_on, right_on, join_type='inner'):
    left = CommonUtils._add_sort_index_if_needed(left, join_type)
    df1 = pd.DataFrame(left)
    df2 = pd.DataFrame(CommonUtils._stringify_dict_values(right))
    df = df1.join(df2.set_index(CommonUtils._lowercase_list_values(right_on)), on=CommonUtils._lowercase_list_values(left_on), how=join_type).fillna('')
    return CommonUtils._sort_results_if_needed(CommonUtils._df_to_dict(df), join_type)

  @staticmethod
  def join_datasets_first_match(left, right, left_on, right_on, join_type='inner'):
    return CommonUtils.join_datasets_unique_match(left, list(reversed(right)), left_on, right_on, join_type)

  @staticmethod
  def join_datasets_all_matches(left, right, left_on, right_on, join_type='inner'):
    left = CommonUtils._add_sort_index_if_needed(left, join_type)
    df1 = pd.DataFrame(left)
    df2 = pd.DataFrame(CommonUtils._stringify_dict_values(right))
    df = pd.merge(df1, df2, how=join_type, left_on=CommonUtils._lowercase_list_values(left_on), right_on=CommonUtils._lowercase_list_values(right_on), sort=False).fillna('')
    return CommonUtils._sort_results_if_needed(CommonUtils._df_to_dict(df), join_type)

  @staticmethod
  def _stringify_dict_values(data):
    items = []
    for row in data:
      row = dict((fieldname.lower(), str(value) if value is not None else '') for (fieldname, value) in row.items() if fieldname)
      items.append(row)
    return items

  @staticmethod
  def _lowercase_list_values(data):
    return data.lower() if isinstance(data, str) else [row.lower() for row in data]

  @staticmethod
  def _df_to_dict(df):
    return list((df.T.to_dict().values())) #list((df.to_dict('index').values()))

  @staticmethod
  def _add_sort_index_if_needed(records, join_type):
    return SortUtils.add_sort_index(records) if join_type == 'inner' and not SortUtils.has_sort_index(records) else records

  @staticmethod
  def _sort_results_if_needed(records, join_type):
    return SortUtils.sort_by_index(records) if join_type == 'inner' else records

#####################
#     Sort Utils    #
#####################
class SortUtils:
  _sort_index = 'sort_index'

  @staticmethod
  def has_sort_index(records):
    return not records or SortUtils._sort_index in records[0]

  @staticmethod
  def add_sort_index(records):
    num_of_digits = len(str(len(records)))
    for index, record in enumerate(records):
      record[SortUtils._sort_index] = str(index).zfill(num_of_digits)
    return records

  @staticmethod
  def sort_by_index(records):
    return sorted(records, key=lambda r: r[SortUtils._sort_index])

#####################
#   Format Utils    #
#####################
class FormatUtils:
  @staticmethod
  def format_date(date, string_format='%Y%m%d'):
    return date.strftime(string_format) if date else None

#####################
#   Group Utils    #
#####################
class GroupUtils:
  @staticmethod
  def group_by_field_name(field_names, target_loop_id, target_loop_x12_node, records):
    result = {}
    for record in records:
      key = ''.join([(str(record[field_name] or '')) for field_name in field_names])
      result.setdefault(key, []).append(record)

    groups = []
    for record_set in list(result.values()):
      groups.append(DataGroup(target_loop_id, target_loop_x12_node, record_set))
    return groups

  @staticmethod # decorator
  def group_by_member_elections(filter_map_func):
    def wrapper(*args, **kwargs):
      records = filter_map_func(*args, **kwargs)
      return GroupUtils.group_by_field_name(['companyuserid', 'division_id', 'employeenumber', 'employee_ssn', 'dependent_ssn', 'relationship', 'employee_first_name', 'employee_last_name', 'employee_middle_name', 'dependent_first_name', 'dependent_last_name', 'dependent_middle_name', 'email'], '2300', X12Node(loop='2300'), records)
    return wrapper

#####################
#   Filter Utils    #
#####################
class FilterUtils:
  @staticmethod
  def remove_empty_records(records, keys_to_check=['employee_ssn']):
    return list(filter(lambda record: all([key in record and record[key] for key in keys_to_check]), records))

  @staticmethod
  def filter_by_benefit_type_and_relationship(records):
    return list(filter(lambda r: int(r['benefit_type'] or 0) <= 10 or int(r['benefit_type'] or 0) >= 14 or not r['relationship'], records))

#####################
#     Map Utils     #
#####################
class MapUtils:
  @staticmethod
  def nullify_empty_values(record):
    for fieldname in record:
      if record[fieldname].upper() == 'NULL' or (record[fieldname] or '').strip() == '':
        record[fieldname] = None

  @staticmethod
  def parse_or_nullify_dates(record, more_date_fields=None):
    for fieldname in record:
      if 'date' in fieldname or fieldname in (['dependentcanceled', 'canceled', 'employee_dob', 'enrollmentmodified'] + (more_date_fields or [])):
        if not record[fieldname] or record[fieldname] == '0':
          record[fieldname] = None
        else:
          record[fieldname] = dateparse(record[fieldname]).replace(tzinfo=None)

  @staticmethod
  def add_relationship_fields(record):
    if record['relationship']:
      record['is_employee'] = False
      record['member_ssn'] = record['dependent_ssn']
      record['member_last_name'] = record['dependent_last_name']
      record['member_first_name'] = record['dependent_first_name']
      record['member_middle_name'] = record['dependent_middle_name']
      record['member_dob'] = record['dateofbirth']
      record['member_gender'] = record['gender']
      record['member_benefit_start_date'] = record['dependent_benefit_effectivedate']
      record['member_benefit_end_date'] = record['dependent_cancellation_date'] or record['cancellationdatetime']
    else:
      record['is_employee'] = True
      record['member_ssn'] = record['employee_ssn']
      record['member_last_name'] = record['employee_last_name']
      record['member_first_name'] = record['employee_first_name']
      record['member_middle_name'] = record['employee_middle_name']
      record['member_dob'] = record['employee_dob']
      record['member_gender'] = record['employee_gender']
      record['member_benefit_start_date'] = record['benefiteffectivedate']
      record['member_benefit_end_date'] = record['cancellationdatetime']

  @staticmethod
  def remove_disabled_emails(record):
    if record['email'] and '.disabled' in record['email'].lower():
      record['email'] = None

  @staticmethod
  def set_coverage_code(record, value=None, custom_map=None, by_dependent_count=False):
    coverage_code = CommonUtils.to_binary(record['employeecoverageelected']) + CommonUtils.to_binary(record['spousecoverageelected']) + CommonUtils.to_binary(record['childcoverageelected'])
    if value:
      record['coverage_code'] = value
    elif custom_map:
      record['coverage_code'] = custom_map[coverage_code]
    elif by_dependent_count:
      record['coverage_code'] = 'EMP' if not record['dependentcount'] or record['dependentcount'] == '0' else 'E6D' if int(record['dependentcount']) > 1 else 'ESP' if CommonUtils.to_boolean(record['spousecoverageelected']) else 'E1D'
    else:
      record['coverage_code'] = {'111': 'FAM', '100': 'EMP', '110': 'ESP', '101': 'ECH'}[coverage_code]

  @staticmethod
  def set_earning_value(record):
    if record['earnings']:
      multiplier = 1 if not float(record['earningsunit'] or '0') else float(record['scheduledweeksperyear']) * float(record['scheduledweeklyhours'])
      earnings = "{:.2f}".format(float(record['earnings']) * multiplier)
      record['earnings_value'] = earnings[:-3] if earnings.endswith('.00') else earnings[:-1] if earnings.endswith('0') else earnings
    else:
      record['earnings_value'] = None

  @staticmethod
  def increment_enrollment_datetime(record):
    if record['enrollmentdatetime']:
      record['enrollmentdatetime'] = record['enrollmentdatetime'] + timedelta(days=1)

  @staticmethod
  def zfill_null_dependent_ssn(record):
    if not record['dependent_ssn']:
      record['dependent_ssn'] = '000000000'
    if not record['is_employee']:
      record['member_ssn'] = record['dependent_ssn']

  @staticmethod
  def set_alternative_cancellation_date(record):
    if record['is_employee']:
      record['member_benefit_end_date'] = record['canceled']
    else:
      record['member_benefit_end_date'] = record['dependentcanceled'] or record['canceled']

  @staticmethod
  def set_contact_codes_and_info(record, include_home_phone, include_work_phone, include_email):
    if not include_home_phone:
      record['homephonenumber'] = None
    if not include_work_phone:
      record['workphonenumber'] = None
    if not include_email:
      record['email'] = None

    record['contact_function_code'] = 'IP' if record['homephonenumber'] or record['workphonenumber'] or record['email'] else None
    record['contact_code_1'] = 'HP' if record['homephonenumber'] else 'WP' if record['workphonenumber'] else 'EM'
    record['contact_info_1'] = record['homephonenumber'] or record['workphonenumber'] or record['email']
    record['contact_code_2'] = 'WP' if record['homephonenumber'] and record['workphonenumber'] else 'EM'
    record['contact_info_2'] = record['workphonenumber'] if record['homephonenumber'] and record['workphonenumber'] else record['email'] if record['homephonenumber'] or record['workphonenumber'] else None
    record['contact_code_3'] = 'EM'
    record['contact_info_3'] = record['email'] if record['homephonenumber'] and record['workphonenumber'] else None

  @staticmethod
  def set_custom_employee_info(record, code, value):
    record['custom_employee_info_code'] = code
    record['custom_employee_info_value'] = value

  @staticmethod
  def set_health_related_code(record, value='U'):
    record['health_related_code'] = value

  @staticmethod
  def set_late_enrollment_indicator(record, value='N'):
    record['late_enrollment_indicator'] = value

  @staticmethod
  def set_medicare_plan_code(record, value='E'):
    record['medicare_plan_code'] = value

  @staticmethod
  def change_employee_number(record, value, identifier=None):
    record['employeenumber'] = value
    if identifier: record['employe_enumber_identifier'] = identifier

  @staticmethod
  def remove_employee_number(record):
    record['employeenumber'] = None

  @staticmethod
  def remove_employment_end_date(record):
    record['statuschangeeffectivedate'] = None

  @staticmethod
  def change_hire_code_and_value(record, code, value):
    record['hire_code'] = code
    record['dateofhire'] = value

  @staticmethod
  def change_insurance_line_code(record, value):
    record['benefit_type'] = value

  @staticmethod
  def remove_date_of_hire(record):
    record['dateofhire'] = None

  @staticmethod
  def set_basic_plan_code(record, value, identifier=None):
    record['basic_plan_code'] = value
    if identifier: record['basic_plan_code_identifier'] = identifier

  @staticmethod
  def set_plan_code(record, value):
    record['carrier_plan'] = value

  @staticmethod
  def set_plan_by_carrier_plan_5(record):
    carrier_plans = (record['carrierplandata5'] or '').split('|')
    record['carrier_plan'] = carrier_plans[0] if record['homeaddress_state'] == 'MI' else carrier_plans[-1]

  @staticmethod
  def set_plan_codes(record, value):
    record['carrier_plans'] = value

  @staticmethod
  def set_employment_status_code(record, value):
    record['employment_status_code'] = value

#####################
#   Query Utils    #
#####################
class QueryUtils:
  @staticmethod
  def get_common_declare_statements(remove_carrier=False):
    statements = r"""
      DECLARE @ONEWEEKAGO DATE = DATEADD(DAY, -{DaysInPast}, GETDATE());
      DECLARE @COMPANYID BIGINT = {companyId};
    """
    if not remove_carrier:
      statements += r"""
        DECLARE @CARRIERID BIGINT = {carrierId};
      """
    statements += r"""
      DECLARE @ENROLLMENTPERIODID BIGINT = {enrollmentPeriodId};
    """
    return statements

  @staticmethod
  def get_common_select_statements():
    return r"""
      SELECT *,
            CASE
              WHEN CancellationDateTime IS NOT NULL
                    AND CancellationDateTime > @ONEWEEKAGO
                    AND CancellationDateTime <= GETDATE() THEN CancellationDateTime
              WHEN CancellationDateTime IS NOT NULL
                    AND CancellationDateTime < @ONEWEEKAGO
                    AND EnrollmentModified > @ONEWEEKAGO THEN CancellationDateTime
              ELSE NULL
            END AS [Canceled],
            CASE
              WHEN CancellationDateTime IS NOT NULL
                    AND CancellationDateTime > @ONEWEEKAGO
                    AND CancellationDateTime <= GETDATE() THEN CancellationDateTime
              WHEN CancellationDateTime IS NOT NULL
                    AND CancellationDateTime < @ONEWEEKAGO
                    AND EnrollmentModified > @ONEWEEKAGO THEN CancellationDateTime
              WHEN CancellationDateTime IS NULL
                    AND Dependent_Cancellation_Date > @ONEWEEKAGO
                    AND Dependent_Cancellation_Date <= GETDATE() THEN
              Dependent_Cancellation_Date
              ELSE NULL
            END AS [DependentCanceled]
    """

  @staticmethod
  def get_common_where_clause(remove_carrier=False):
    return f"""
      WHERE  Company_id = @COMPANYID
            {' AND Carrier_ID = @CARRIERID ' if not remove_carrier else ' '}
            AND EnrollmentPeriod_Id = @ENROLLMENTPERIODID
    """

  @staticmethod
  def get_common_conditions(remove_dependent=False):
    conditions = r"""
      AND ( CancellationDateTime IS NULL
              OR ( CancellationDateTime IS NOT NULL
                  AND CancellationDateTime > @ONEWEEKAGO
                  AND CancellationDateTime <= GETDATE() )
              OR ( CancellationDateTime IS NOT NULL
                  AND CancellationDateTime < @ONEWEEKAGO
                  AND EnrollmentModified > @ONEWEEKAGO )
              OR ( CancellationDateTime IS NOT NULL
                  AND CancellationDateTime >= GETDATE() ) )
    """
    if not remove_dependent:
      conditions += r"""
        AND ( Dependent_Cancellation_Date IS NULL
                OR ( Dependent_Cancellation_Date IS NOT NULL
                    AND Dependent_Cancellation_Date > @ONEWEEKAGO
                    AND Dependent_Cancellation_Date <= GETDATE() )
                OR ( Dependent_Cancellation_Date IS NOT NULL
                    AND Dependent_Cancellation_Date < @ONEWEEKAGO
                    AND EnrollmentModified > @ONEWEEKAGO )
                OR ( Dependent_Cancellation_Date IS NOT NULL
                    AND Dependent_Cancellation_Date >= GETDATE() ) )
      """
    conditions += r"""
        AND BenefitEffectiveDate IS NOT NULL
    """
    return conditions

  @staticmethod
  def get_benefit_type_condition(valid_types_list):
    return f"""
      AND Benefit_Type IN ( {', '.join(map(str, valid_types_list))} )
    """

  @staticmethod
  def get_division_id_condition(value):
    return f"""
      AND Division_Id = {value}
    """

  @staticmethod
  def get_from_statement_no_condition():
    return r"""
      FROM   [iris-production].[dbo].[MemberElectionView]
    """

  @staticmethod
  def get_from_statement_by_division():
    return QueryUtils.get_from_statement_no_condition() + r"""
            OUTER APPLY (SELECT TOP (1) NAME [DivisionName]
                          FROM   Divisions
                          WHERE  Id = MemberElectionView.Division_Id) Loc
    """

  @staticmethod
  def get_from_statement_by_outer_location():
    return QueryUtils.get_from_statement_no_condition() + r"""
            OUTER APPLY (SELECT TOP (1) Location_Id
                          FROM   LocationHistories
                          WHERE  CompanyUser_id = MemberElectionView.CompanyUserId
                          ORDER  BY LocationEffectiveDate DESC) Loc
    """

  @staticmethod
  def get_from_statement_by_corss_location():
    return QueryUtils.get_from_statement_no_condition() + r"""
            CROSS APPLY (SELECT TOP (1) Location_Id
                          FROM   LocationHistories
                          WHERE  CompanyUser_id = MemberElectionView.CompanyUserId
                          ORDER  BY LocationEffectiveDate DESC) Loc
    """

  @staticmethod
  def get_from_statement_by_dependent_count():
    return QueryUtils.get_from_statement_no_condition() + r"""
            OUTER APPLY (SELECT Count(*) DependentCount
                          FROM   FamilyMemberEnrollments fme
                          WHERE  fme.Enrollment_Id = MemberElectionView.Enrollment_Id
                                AND ( BenefitEndDate IS NULL
                                        OR ( BenefitEndDate IS NOT NULL
                                            AND BenefitEndDate > @ONEWEEKAGO
                                            AND BenefitEndDate <= GETDATE() )
                                        OR ( BenefitEndDate IS NOT NULL
                                            AND BenefitEndDate < @ONEWEEKAGO
                                            AND ModifiedAt > @ONEWEEKAGO )
                                        OR ( BenefitEndDate IS NOT NULL
                                            AND BenefitEndDate >= GETDATE() ) ))
                        DepCount
    """
  @staticmethod
  def get_ordeby_ssn_statement_only():
    return r"""
      ORDER  BY Employee_SSN DESC
    """

  @staticmethod
  def get_ordeby_ssn_statement(include_dob=False, include_last_name=False, replace_null_dependent_ssn=False, descending_benefit_type=False):
    return QueryUtils.get_ordeby_ssn_statement_only() +  f"""
            , ( CASE
                  WHEN Relationship IS NULL THEN 0
                  ELSE 1
                END ),
              {"ISNULL(Dependent_SSN, '999999999')" if replace_null_dependent_ssn else "Dependent_SSN"} DESC,
              {'Dependent_Last_Name DESC,' if include_last_name else ''}
              Dependent_First_Name DESC,
              {'DateOfBirth,' if include_dob else ''}
              Benefit_Type {'DESC' if descending_benefit_type else ''}
    """

  @staticmethod
  def get_ordeby_name_statement():
    return r"""
      --ORDER BY Employee_Last_Name, Employee_First_Name,  Dependent_SSN DESC, Dependent_Last_Name DESC, Dependent_First_Name DESC, Benefit_Type
    """

  @staticmethod
  def get_orderby_name_and_relationship():
    return r"""
      --ORDER by Employee_Last_Name DESC, (CASE WHEN Relationship IS NULL THEN 0 ELSE 1 END), Dependent_SSN DESC, Dependent_Last_Name DESC, Dependent_First_Name DESC, Benefit_Type
    """

  @staticmethod
  def get_orderby_name_and_relationship_and_dob():
    return r"""
      --ORDER by Employee_Last_Name, Employee_First_Name, (CASE WHEN Relationship IS NULL THEN 0 ELSE 1 END), Dependent_SSN DESC, Dependent_Last_Name DESC, Dependent_First_Name DESC, DateOfBirth, Benefit_Type DESC
    """