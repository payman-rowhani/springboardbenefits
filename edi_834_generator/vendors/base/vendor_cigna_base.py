import vendors.base.vendor_base as base
import vendors.base.common as common

# EDI Map: CignaXMLto834.xml
class VendorCignaBase(base.VendorBase):
  def get_root_info_map(self, sender_id=None, sender_tax_id=None, sender_name=None, **kwargs):
    info = super().get_root_info_map(
      sender_id=sender_id,
      sender_name=sender_name,
      sender_tax_id=sender_tax_id,
      receiver_id='029053964',
      receiver_name='CIGNA',
      receiver_code='06-0303370',
      ts_ref_number='S20170124141107',
      master_policy_id='RLHA',
      application_sender_code=sender_id,
      application_receiver_code='029053964',
      entity_identifier_code='BO',
      repetition_separator='^',
      usage_indicator='P',
      first_interchange_id_qualifier='30',
      second_interchange_id_qualifier='20'
    )
    if kwargs: info.update(kwargs)
    return info

  @common.GroupUtils.group_by_member_elections
  def filter_and_map(self, records):
    records = self._apply_general_filtering(records)
    records = self.child_class_filter_and_map(records)
    return records

  def _apply_general_mapping(self, record, carrier_plan, carrier_plans, leave_dependent_ssn_unchanged=False, keep_employment_end_date=False, use_alt_cancel_time=True):
    super()._apply_general_mapping(record, use_alt_cancel_time=use_alt_cancel_time)
    common.MapUtils.increment_enrollment_datetime(record)
    common.MapUtils.set_contact_codes_and_info(record, include_home_phone=True, include_work_phone=True, include_email=True)
    common.MapUtils.remove_employee_number(record)
    common.MapUtils.set_employment_status_code(record, 'PT')
    common.MapUtils.set_coverage_code(record)
    common.MapUtils.set_plan_code(record, carrier_plan)
    common.MapUtils.set_plan_codes(record, carrier_plans)
    if not leave_dependent_ssn_unchanged: common.MapUtils.zfill_null_dependent_ssn(record)
    if not keep_employment_end_date: common.MapUtils.remove_employment_end_date(record)

  def child_class_filter_and_map(self, records):
    raise NotImplementedError('filter_and_map not implemented!')