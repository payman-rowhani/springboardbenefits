import vendors.base.vendor_base as base
import vendors.base.common as common

# EDI Map: AnthemXMLto834.xml
class VendorAnthemBase(base.VendorBase):
  def get_root_info_map(self, sender_id=None, **kwargs):
    info = super().get_root_info_map(
      sender_id=sender_id,
      sender_name='FELLFAB CORPORATION',
      sender_tax_id='760705525',
      receiver_id='BCCAWGS',
      receiver_name='ANTHEM BLUE CROSS',
      receiver_code='953760001',
      ts_ref_number='S20170124141107',
      master_policy_id=None,
      application_sender_code=sender_id,
      application_receiver_code='BCCAWGS',
      entity_identifier_code='TV',
      repetition_separator='|',
      usage_indicator='T',
      first_interchange_id_qualifier='ZZ',
      second_interchange_id_qualifier='ZZ'
    )
    if kwargs: info.update(kwargs)
    return info

  def filter_and_map(self, records):
    records = self._apply_general_filtering(records)
    records = self.child_class_filter_and_map(records)
    return records

  def _apply_general_mapping(self, record, basic_plan_code, set_custom_info=True):
    super()._apply_general_mapping(record)
    common.MapUtils.set_contact_codes_and_info(record, include_home_phone=False, include_work_phone=False, include_email=True)
    common.MapUtils.remove_employee_number(record)
    common.MapUtils.set_employment_status_code(record, 'PT')
    common.MapUtils.set_basic_plan_code(record, basic_plan_code)
    common.MapUtils.remove_employment_end_date(record)
    if set_custom_info: common.MapUtils.set_custom_employee_info(record, 'DX', record['homeaddress_state'])

  def child_class_filter_and_map(self, records):
    raise NotImplementedError('filter_and_map not implemented!')