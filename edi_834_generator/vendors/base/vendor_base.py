import os.path
from abc import ABC, abstractmethod
from datetime import datetime

import vendors.base.common as common
from core.data_field import DataField
from core.x12_node import X12Node

class VendorBase(ABC):
  def __init__(self, now=None, db_context=None, data_folder_path=None, file_name_prefix=None, delivery_name=None, is_production=False, output_edi_path=None, delivery_directory=None, bucket_name=None):
    self.now = now or datetime.utcnow()

    default_context = self.get_default_context_variables()
    self.db_context = self._handle_data_dict_operations(default_context['db_context'], db_context)
    self.data_folder_path = data_folder_path or default_context['data_folder_path']
    self.file_name_prefix = file_name_prefix or default_context['file_name_prefix']
    self.delivery_name = delivery_name or (default_context['delivery_name'] or '').replace('date', self.now.strftime("%Y%m%d"))
    self.is_production = is_production or default_context['is_production']

    self.edi_file_path = output_edi_path or os.path.join(self.data_folder_path, f'{self.file_name_prefix}_{self.now.strftime("%Y%m%d")}.834')
    self.bucket_name = bucket_name or ("iris-benefits-secure-production" if self.is_production else "iris-benefits-secure-local")
    self.bucket_directory = delivery_directory or (f'export/{self.db_context["companyId"]}/{self.db_context["carrierId"]}/toprocess' if 'companyId' in self.db_context else None)
    self.bucket_file_key = f'{self.bucket_directory}/{self.delivery_name}' if self.delivery_name and self.bucket_directory else None

  # root info (values will replace <XML_ROOT_TEMPLATE_PATH>)
  @abstractmethod
  def get_root_info_map(self,
    sender_id=None,
    sender_name=None,
    sender_tax_id=None,
    receiver_id=None,
    receiver_name=None,
    receiver_code=None,
    ts_ref_number=None,
    master_policy_id=None,
    application_sender_code=None,
    application_receiver_code=None,
    entity_identifier_code=None,
    repetition_separator=None,
    usage_indicator=None,
    first_interchange_id_qualifier=None,
    second_interchange_id_qualifier=None,
    action_code='4',
    compele_separator=':',
    eol=''
  ):
    return {
      'sender_id': sender_id,
      'sender_name': sender_name,
      'sender_tax_id': sender_tax_id,
      'receiver_id': receiver_id,
      'receiver_name': receiver_name,
      'receiver_code': receiver_code,
      'ts_ref_number': ts_ref_number,
      'master_policy_id': master_policy_id,
      'application_sender_code': application_sender_code,
      'application_receiver_code': application_receiver_code,
      'entity_identifier_code': entity_identifier_code,
      'repetition_separator': repetition_separator,
      'usage_indicator': usage_indicator,
      'first_interchange_id_qualifier': first_interchange_id_qualifier,
      'second_interchange_id_qualifier': second_interchange_id_qualifier,
      'action_code': action_code,
      'compele_separator': compele_separator,
      'eol': eol
    }

  # define filtering and mapping here
  @abstractmethod
  def filter_and_map(self, records):
    records = self._apply_general_filtering(records)
    for record in records:
      self._apply_general_mapping(record)
    return records

  # define query here
  @abstractmethod
  def get_default_context_variables(self):
    pass

  # define query here
  @abstractmethod
  def get_db_query(self, raw=False):
    pass

  # root (header and footer) xml template path
  def get_root_template_path(self):
    return 'input_xml_templates/834_root_template.xml'

  # details (member or records) xml template paths
  def get_details_template_path(self):
    return 'input_xml_templates/834_details_template.xml'

  # root data fields (X12 nodes are searched in <XML_ROOT_TEMPLATE_PATH>)
  def get_root_data_fields(self, override_map={}, keys_to_delete=None, convert_to_list=False):
    root_info = self.get_root_info_map()
    root_fields_map = {
      'first_interchange_id_qualifier': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=5), default_val=root_info['first_interchange_id_qualifier']),
      'sender_id': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=6), val_len=15, default_val=root_info['sender_id']),
      'second_interchange_id_qualifier': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=7), default_val=root_info['second_interchange_id_qualifier']),
      'receiver_id': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=8), val_len=15, default_val=root_info['receiver_id']),
      'date1': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=9), default_val=self.now.strftime('%y%m%d')), # interchange date
      'time1': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=10), default_val=self.now.strftime('%H%M')), # interchange time
      'repetition_separator': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=11), default_val=root_info['repetition_separator']),
      'usage_indicator': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=15), default_val=root_info['usage_indicator']),
      'compele_separator': DataField(None, X12Node(loop='ISA_LOOP', seg='ISA', ele_index=16), default_val=root_info['compele_separator']),
      'application_sender_code': DataField(None, X12Node(loop='GS_LOOP', seg='GS', ele_index=2, parent=X12Node(loop='ISA_LOOP')), default_val=root_info['application_sender_code']),
      'application_receiver_code': DataField(None, X12Node(loop='GS_LOOP', seg='GS', ele_index=3, parent=X12Node(loop='ISA_LOOP')), default_val=root_info['application_receiver_code']),
      'date2': DataField(None, X12Node(loop='GS_LOOP', seg='GS', ele_index=4, parent=X12Node(loop='ISA_LOOP')), default_val=self.now.strftime('%Y%m%d')),
      'time2': DataField(None, X12Node(loop='GS_LOOP', seg='GS', ele_index=5, parent=X12Node(loop='ISA_LOOP')), default_val=self.now.strftime('%H%M%S%f')[:-4]),
      'ts_ref_number': DataField(None, X12Node(loop='HEADER', seg='BGN', ele_index=2, parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP')))), default_val=root_info['ts_ref_number']),
      'date3': DataField(None, X12Node(loop='HEADER', seg='BGN', ele_index=3, parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP')))), default_val=self.now.strftime('%Y%m%d')),
      'time3': DataField(None, X12Node(loop='HEADER', seg='BGN', ele_index=4, parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP')))), default_val=self.now.strftime('%H%M%S%f')[:-4]),
      'action_code': DataField(None, X12Node(loop='HEADER', seg='BGN', ele_index=8, parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP')))), default_val=root_info['action_code']),
      'master_policy_id': DataField(None, X12Node(loop='HEADER', seg='REF', ele_index=2, parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))), delete_parent=True), default_val=root_info['master_policy_id'], deletable=True),
      'date4': DataField(None, X12Node(loop='HEADER', seg='DTP', ele_index=3, parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))), delete_parent=True), default_val=self.now.strftime('%Y%m%d') if root_info['master_policy_id'] else None, deletable=True), # date time period
      'sender_name': DataField(None, X12Node(loop='1000A', seg='N1', ele_index=2, parent=X12Node(loop='HEADER', parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))))), default_val=root_info['sender_name']),
      'sender_tax_id': DataField(None, X12Node(loop='1000A', seg='N1', ele_index=4, parent=X12Node(loop='HEADER', parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))))), default_val=root_info['sender_tax_id']),
      'receiver_name': DataField(None, X12Node(loop='1000B', seg='N1', ele_index=2, parent=X12Node(loop='HEADER', parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))))), default_val=root_info['receiver_name']),
      'receiver_code': DataField(None, X12Node(loop='1000B', seg='N1', ele_index=4, parent=X12Node(loop='HEADER', parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))))), default_val=root_info['receiver_code']),
      'entity_identifier_code': DataField(None, X12Node(loop='1000C', seg='N1', ele_index=1, parent=X12Node(loop='HEADER', parent=X12Node(loop='ST_LOOP', parent=X12Node(loop='GS_LOOP', parent=X12Node(loop='ISA_LOOP'))))), default_val=root_info['entity_identifier_code']),
    }
    return self._handle_data_dict_operations(root_fields_map, override_map, keys_to_delete, convert_to_list)

  # details data fields map to xml element (X12 nodes are searched in <XML_DETAILS_TEMPLATE_PATH>)
  def get_details_data_fields(self, override_map={}, keys_to_delete=None, convert_to_list=False):
    data_fields_map = {
      'is_employee': DataField('is_employee', X12Node(seg='INS', ele_index=1), val_formatter=lambda is_employee: 'N' if not is_employee else 'Y'),
      'relationship': DataField('relationship', X12Node(seg='INS', ele_index=2), val_formatter=lambda relationship: '18' if not relationship else '01' if relationship in ['1', '8'] else '19'),
      'medicare_plan_code': DataField('medicare_plan_code', X12Node(seg='INS', ele_index=6, subele_index=1, delete_parent=True), deletable=True),
      'employment_status_code': DataField('employment_status_code', X12Node(seg='INS', ele_index=8), deletable=True, should_delete_func=lambda rval,fval,data: not data['is_employee']),
      'fulltime_student': DataField('fulltimestudent', X12Node(seg='INS', ele_index=9), val_formatter={'1': 'F', '0': 'N', 'true': 'F', 'false': 'N'}, default_val='N', deletable=True, should_delete_func=lambda rval,fval,data: data['is_employee'] or data['relationship'] in ['1', '8']),
      'is_disabled': DataField('isdisabled', X12Node(seg='INS', ele_index=10), val_formatter={'1': 'Y', '0': 'N', 'true': 'Y', 'false': 'N'}, default_val='N', deletable=True, should_delete_func=lambda rval,fval,data: data['is_employee']),
      'employee_ssn': DataField('employee_ssn', X12Node(seg='REF', seg_pos=2, ele_index=2)),
      'custom_employee_info_code': DataField('custom_employee_info_code', X12Node(seg='REF', seg_pos=3, ele_index=1, delete_parent=True), default_val='ABB', deletable=True),
      'custom_employee_info_value': DataField('custom_employee_info_value', X12Node(seg='REF', seg_pos=3, ele_index=2, delete_parent=True), deletable=True),
      'employe_enumber_identifier': DataField('employe_enumber_identifier', X12Node(seg='REF', seg_pos=4, ele_index=1), default_val='23'),
      'employe_enumber': DataField('employeenumber', X12Node(seg='REF', seg_pos=4, ele_index=2, delete_parent=True), deletable=True),
      'basic_plan_code_identifier': DataField('basic_plan_code_identifier', X12Node(seg='REF', seg_pos=5, ele_index=1), default_val='1L'),
      'basic_plan_code': DataField('basic_plan_code', X12Node(seg='REF', seg_pos=5, ele_index=2, delete_parent=True), deletable=True),
      'hire_code': DataField('hire_code', X12Node(seg='DTP', seg_pos=6, ele_index=1, delete_parent=True), default_val='336', deletable=True),
      'date_of_hire': DataField('dateofhire', X12Node(seg='DTP', seg_pos=6, ele_index=3, delete_parent=True), val_formatter=common.FormatUtils.format_date, deletable=True),
      'employment_end_date': DataField('statuschangeeffectivedate', X12Node(seg='DTP', seg_pos=7, ele_index=3, delete_parent=True), val_formatter=common.FormatUtils.format_date, deletable=True),
      'member_last_name': DataField('member_last_name', X12Node(loop='2100A', seg='NM1', ele_index=3), deletable=True),
      'member_first_name': DataField('member_first_name', X12Node(loop='2100A', seg='NM1', ele_index=4), deletable=True),
      'member_middle_name': DataField('member_middle_name', X12Node(loop='2100A', seg='NM1', ele_index=5), deletable=True),
      'member_ssn': DataField('member_ssn', X12Node(loop='2100A', seg='NM1', ele_index=9, related_nodes=[X12Node(loop='2100A', seg='NM1', ele_index=8)]), deletable=True, should_delete_func=lambda rval,fval,data: not data['is_employee'] and not data['dependent_ssn']),
      'contact_function_code': DataField('contact_function_code', X12Node(loop='2100A', seg='PER', ele_index=1, delete_parent=True), deletable=True),
      'contact_code_1': DataField('contact_code_1', X12Node(loop='2100A', seg='PER', ele_index=3)),
      'contact_info_1': DataField('contact_info_1', X12Node(loop='2100A', seg='PER', ele_index=4, related_nodes=[X12Node(loop='2100A', seg='PER', ele_index=3)]), deletable=True),
      'contact_code_2': DataField('contact_code_2', X12Node(loop='2100A', seg='PER', ele_index=5)),
      'contact_info_2': DataField('contact_info_2', X12Node(loop='2100A', seg='PER', ele_index=6, related_nodes=[X12Node(loop='2100A', seg='PER', ele_index=5)]), deletable=True),
      'contact_code_3': DataField('contact_code_3', X12Node(loop='2100A', seg='PER', ele_index=7)),
      'contact_info_3': DataField('contact_info_3', X12Node(loop='2100A', seg='PER', ele_index=8, related_nodes=[X12Node(loop='2100A', seg='PER', ele_index=7)]), deletable=True),
      'address_line_1': DataField('homeaddress_streetaddress1', X12Node(loop='2100A', seg='N3', ele_index=1)),
      'address_line_2': DataField('homeaddress_streetaddress2', X12Node(loop='2100A', seg='N3', ele_index=2), deletable=True),
      'city': DataField('homeaddress_city', X12Node(loop='2100A', seg='N4', ele_index=1)),
      'state': DataField('homeaddress_state', X12Node(loop='2100A', seg='N4', ele_index=2)),
      'zipcode': DataField('homeaddress_postalcode', X12Node(loop='2100A', seg='N4', ele_index=3)),
      'member_dob': DataField('member_dob', X12Node(loop='2100A', seg='DMG', ele_index=2), val_formatter=common.FormatUtils.format_date, deletable=True),
      'member_gender': DataField('member_gender', X12Node(loop='2100A', seg='DMG', ele_index=3), val_formatter={'1': 'M', '2': 'F'}, default_val='U'),
      'earnings': DataField('earnings_value', X12Node(loop='2100A', seg='ICM', ele_index=2, delete_parent=True), deletable=True),
      'health_related_code': DataField('health_related_code', X12Node(loop='2100A', seg='HLH', ele_index=1, delete_parent=True), deletable=True),
      'insurance_code': DataField('benefit_type', X12Node(loop='2300', seg='HD', ele_index=3), val_formatter={'1': 'HLT', '2': 'DEN', '3': 'VIS', '11': 'DCP', '13': 'MM', '21': 'AH', '22': 'AG'}),
      'plan_code': DataField('carrier_plan', X12Node(loop='2300', seg='HD', ele_index=4), deletable=True),
      'coverage_code': DataField('coverage_code', X12Node(loop='2300', seg='HD', ele_index=5), deletable=True),
      'late_enrollment_indicator': DataField('late_enrollment_indicator', X12Node(loop='2300', seg='HD', ele_index=9), deletable=True),
      'member_benefit_start_date': DataField('member_benefit_start_date', X12Node(loop='2300', seg='DTP', seg_pos=2, ele_index=3, delete_parent=True), val_formatter=common.FormatUtils.format_date, deletable=True),
      'member_benefit_end_date': DataField('member_benefit_end_date', X12Node(loop='2300', seg='DTP', seg_pos=3, ele_index=3, delete_parent=True), val_formatter=common.FormatUtils.format_date, deletable=True, should_delete_func=lambda rval,fval,data: rval > self.now or (not data['dependentcanceled'] and not data['canceled'])),
      'plan_codes': DataField('carrier_plans', X12Node(loop='2300', seg='REF', seg_pos=4, ele_index=2, delete_parent=True), deletable=True),
    }
    return self._handle_data_dict_operations(data_fields_map, override_map, keys_to_delete, convert_to_list)

  # output edi full local file path
  def get_edi_file_path(self):
    return self.edi_file_path

  # delivery bucket and key on s3
  def get_delivery_bucket_and_file(self):
    return self.bucket_name, self.bucket_file_key

  # should put result on bucket?
  def should_put_file_on_bucket(self):
    return bool((self.bucket_name or '').strip() and (self.bucket_file_key or '').strip())

  def _apply_general_mapping(self, record, use_alt_cancel_time=True):
    common.MapUtils.nullify_empty_values(record)
    common.MapUtils.remove_disabled_emails(record)
    common.MapUtils.parse_or_nullify_dates(record)
    common.MapUtils.add_relationship_fields(record)
    if use_alt_cancel_time: common.MapUtils.set_alternative_cancellation_date(record)

  def _apply_general_filtering(self, records):
    records = common.FilterUtils.remove_empty_records(records)
    return records

  def _handle_data_dict_operations(self, data_dict, override_map={}, keys_to_delete=None, convert_to_list=False):
    if override_map:
      data_dict.update(override_map)

    if keys_to_delete:
      for key in keys_to_delete:
        if key in data_dict: del data_dict[key]

    return data_dict if not convert_to_list else list(data_dict.values())