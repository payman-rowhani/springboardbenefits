import vendors.base.common as common
import vendors.base.vendor_base as base

# EDI Map: ESXMLto834.xml
class VENDOR_EXPRESSSCRIPTS_834_SPRINGFIELD(base.VendorBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='370701328',
      sender_name='Springfield Clinic',
      sender_tax_id='370701328',
      receiver_id='223461740',
      receiver_name='Express Scripts',
      receiver_code='223461740',
      ts_ref_number='S20170124141107',
      master_policy_id=None,
      application_sender_code='370701328',
      application_receiver_code='5811',
      entity_identifier_code='BO',
      repetition_separator='|',
      usage_indicator='P',
      first_interchange_id_qualifier='ZZ',
      second_interchange_id_qualifier='ZZ',
      eol='\n'
    )

  @common.GroupUtils.group_by_member_elections
  def filter_and_map(self, records):
    records = self._apply_general_filtering(records)

    plan_code_translation = [
      {'planid': '602', 'plancode': 'SPRNGCLINCPPOA'},
      {'planid': '598', 'plancode': 'SPRNGCLINCHDHPA'},
      {'planid': '600', 'plancode': 'SPRNGCLINCPPOA'},
      {'planid': '596', 'plancode': 'SPRNGCLINCHDHPA'},
      {'planid': '599', 'plancode': 'SPRNGCLINCPPOA'},
      {'planid': '590', 'plancode': 'SPRNGCLINCHDHPA'},
      {'planid': '601', 'plancode': 'SPRNGCLINCPPOA'}
    ]
    records = common.CommonUtils.join_datasets_first_match(records, plan_code_translation, 'planvariant_id', 'planid', 'left')

    for record in records:
      self._apply_general_mapping(record)
      common.MapUtils.set_contact_codes_and_info(record, include_home_phone=False, include_work_phone=False, include_email=True)
      common.MapUtils.set_basic_plan_code(record, record['plancode'])
      common.MapUtils.set_plan_codes(record, record['plancode'])
      common.MapUtils.remove_employee_number(record)
      common.MapUtils.remove_date_of_hire(record)
      common.MapUtils.remove_employment_end_date(record)
      common.MapUtils.set_employment_status_code(record, 'PT')
      common.MapUtils.change_insurance_line_code(record, 'PDG')
      if record['is_employee']: common.MapUtils.set_coverage_code(record)

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 126, "carrierId": 44, "enrollmentPeriodId": 48, "DaysInPast": 7},
      "data_folder_path": "C:\\TalendDataSpace\\ExpressScriptsEDI\\Springfield",
      "file_name_prefix": "ExpressScripts_Springfield_Data",
      "delivery_name": "PSHRCI.NED5811D.UXMITSUB.EDIFILEdata.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements(remove_carrier=True) + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_dependent_count() + \
            common.QueryUtils.get_common_where_clause(remove_carrier=True) + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([1]) + \
            common.QueryUtils.get_orderby_name_and_relationship()
    return query.format(**self.db_context) if not raw else query