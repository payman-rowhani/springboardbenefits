import vendors.base.common as common
import vendors.base.vendor_cigna_base as base

class VENDOR_CIGNA_MULTICOVERAGE_834_SPRINGFIELD(base.VendorCignaBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='370701328',
      sender_tax_id='751803743',
      sender_name='Springfield Clinic'
    )

  def child_class_filter_and_map(self, records):
    variant_code_translations = [
      {'planid': '590', 'plancode': 'HSAI', 'familyplancode': 'HSAF'},
      {'planid': '596', 'plancode': 'HSAI', 'familyplancode': 'HSAF'},
      {'planid': '599', 'plancode': 'OAP', 'familyplancode': 'OAP'},
      {'planid': '600', 'plancode': 'OAP', 'familyplancode': 'OAP'},
      {'planid': '603', 'plancode': 'HCFSA', 'familyplancode': 'HCFSA'},
      {'planid': '597', 'plancode': 'HSAI', 'familyplancode': 'HSAF'},
      {'planid': '598', 'plancode': 'HSAI', 'familyplancode': 'HSAF'},
      {'planid': '601', 'plancode': 'OAP', 'familyplancode': 'OAP'},
      {'planid': '602', 'plancode': 'OAP', 'familyplancode': 'OAP'},
      {'planid': '592', 'plancode': 'DCFSA', 'familyplancode': 'DCFSA'}
    ]
    records = common.CommonUtils.join_datasets_first_match(records, variant_code_translations, 'planvariant_id', 'planid')

    benefit_data_format_code = [
      {'benefittype': '1', 'planforomat': ''},
      {'benefittype': '12', 'planforomat': ''}
    ]
    records = common.CommonUtils.join_datasets_first_match(records, benefit_data_format_code, 'benefit_type', 'benefittype')

    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      carrier_plan = (record['planforomat'] or '').format(record['benefiteffectivedate'], record['employeryearlycontribution'], record['employeeyearlycontribution'])
      _merged_plans = record['familyplancode'] if common.CommonUtils.to_boolean(record['spousecoverageelected']) or common.CommonUtils.to_boolean(record['childcoverageelected']) else record['plancode'] + ('NC' if record['homeaddress_state'] in ['CA', 'NC'] else '')
      carrier_plans = (record['carrierplandata1'] or '') + (record['carrierplandata2'] or '') + _merged_plans
      self._apply_general_mapping(record, carrier_plan, carrier_plans)

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 126, "carrierId": 2, "enrollmentPeriodId": 48, "DaysInPast": 14},
      "data_folder_path": "C:\\TalendDataSpace\\CignaEDI\\Springfield",
      "file_name_prefix": "Cigna_Springfield_Core_Data",
      "delivery_name": "XO16000__xo10001i.57484.date.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_ordeby_ssn_statement(include_last_name=True)
    return query.format(**self.db_context) if not raw else query