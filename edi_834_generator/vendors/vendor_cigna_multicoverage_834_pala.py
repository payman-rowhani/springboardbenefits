import vendors.base.common as common
import vendors.base.vendor_cigna_base as base

class VENDOR_CIGNA_MULTICOVERAGE_834_PALA(base.VendorCignaBase):
  def get_root_info_map(self):
    return super().get_root_info_map(
      sender_id='720932277',
      sender_tax_id='720932277',
      sender_name='PALA'
    )

  def child_class_filter_and_map(self, records):
    division_translations = [
      {'divisionid': '154', 'divisioncode': 'COMP21'},
      {'divisionid': '155', 'divisioncode': 'COMP41'},
      {'divisionid': '156', 'divisioncode': 'COMP62'},
      {'divisionid': '157', 'divisioncode': 'COMP70'},
    ]
    records = common.CommonUtils.join_datasets_first_match(records, division_translations, 'division_id', 'divisionid', 'left')

    variant_translation = [
      {'pvid': '740', 'pcode': 'VISN1'},
      {'pvid': '741', 'pcode': 'DENT1'},
      {'pvid': '742', 'pcode': 'DENT'},
    ]
    records = common.CommonUtils.join_datasets_first_match(records, variant_translation, 'planvariant_id', 'pvid', 'left')

    records = common.FilterUtils.filter_by_benefit_type_and_relationship(records)

    for record in records:
      carrier_plan = record['carrierplandata4']
      _division_code = "Unknow" if not record['divisioncode'] else record['divisioncode']
      carrier_plans = (record['carrierplandata1'] or '') + _division_code + record['pcode']
      self._apply_general_mapping(record, carrier_plan, carrier_plans)

    return records

  def get_default_context_variables(self):
    return {
      "db_context": {"companyId": 147, "carrierId": 2, "enrollmentPeriodId": 77, "DaysInPast": 8},
      "data_folder_path": "C:\\TalendDataSpace\\CignaEDI\\PALA",
      "file_name_prefix": "Cigna_PALA_Data",
      "delivery_name": "XO16000__xo10001i.60380.date.txt",
      "is_production": False
    }

  def get_db_query(self, raw=False):
    query = common.QueryUtils.get_common_declare_statements() + \
            common.QueryUtils.get_common_select_statements() + \
            common.QueryUtils.get_from_statement_by_outer_location() + \
            common.QueryUtils.get_common_where_clause() + \
            common.QueryUtils.get_common_conditions() + \
            common.QueryUtils.get_benefit_type_condition([2 ,3]) + \
            common.QueryUtils.get_ordeby_ssn_statement(include_last_name=True, replace_null_dependent_ssn=True)
    return query.format(**self.db_context) if not raw else query