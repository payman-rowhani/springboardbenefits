# EDI 834 Generator
Contains Python scripts for converting **DB/CSV** to **X12 834 EDI**

Requires **Python 3.6+**

## Install

`pip install -r requirements.txt`

## Create Standalone Package (Useful for AWS Lambda function)

`mkdir package`

`cd package`

`pip install -r requirements.txt -t .`

## Run

### Converting from DB to 834

#### Usage
`convert_db_to_834.py --vendor VENDOR [-h]
                            [--data-folder-path DATA_FOLDER_PATH]
                            [--file-name-prefix FILE_NAME_PREFIX]
                            [--delivery-name DELIVERY_NAME] [--is-production]
                            [--output-edi-path OUTPUT_EDI_PATH]
                            [--delivery-directory DELIVERY_DIRECTORY]
                            [--bucket-name BUCKET_NAME]
                            [--xml-output INTERMEDIATE_XML_OUTPUT_PATH] [--db-server DB_SERVER]
                            [--db-username DB_USERNAME]
                            [--db-password DB_PASSWORD] [--db-name DB_NAME]
                            [--db-port DB_PORT]
                            [--db-context [DB_CONTEXT [DB_CONTEXT ...]]]
                            [--aws-region AWS_REGION_NAME]
                            [--aws-access-key AWS_ACCESS_KEY]
                            [--aws-secret-key AWS_SECRET_KEY]`

#### Description of command-line arguments
    mandatory arguments:
      --vendor              {AETNA_MEDICAL_834_FIS,
                             ANTHEM_LIFE_834_FELLFAB,
                             ANTHEM_MULTICOVERAGE_834_ASMO,
                             ANTHEM_MULTICOVERAGE_834_FELLFAB,
                             CIGNA_DENTAL_834_ASMO,
                             CIGNA_MULTICOVERAGE_834_ASMO,
                             CIGNA_MULTICOVERAGE_834_COLTENE,
                             CIGNA_MULTICOVERAGE_834_CW,
                             CIGNA_MULTICOVERAGE_834_HFHI,
                             CIGNA_MULTICOVERAGE_834_NBOI,
                             CIGNA_MULTICOVERAGE_834_PALA,
                             CIGNA_MULTICOVERAGE_834_SPRINGFIELD,
                             DELTADENTAL_MULTICOVERAGE_834_SPRINGFIELD,
                             EMBLEM_834_ARCHCARE,
                             EXPRESSSCRIPTS_834_SPRINGFIELD,
                             UCH_834_ASMO,
                             VSP_VISION_834_ARCHCARE}
                            Receiver-sender combination code, such as UHC_834_ASMO
                            (same as Talend job names)

    optional arguments:
      -h, --help            show this help message and exit
      --data-folder-path DATA_FOLDER_PATH
                            Path to data folder for local files (if not specified,
                            default value for the vendor will be used)
      --file-name-prefix FILE_NAME_PREFIX
                            File name prefix for local files (if not specified,
                            default value for the vendor will be used)
      --delivery-name DELIVERY_NAME
                            Delivery name to put on bucket (if not specified,
                            default value for the vendor will be used)
      --is-production       Flag for production environment (if not specified,
                            default value for the vendor will be used)
      --output-edi-path OUTPUT_EDI_PATH
                            Path to the output x12 834 edi file (if not specified,
                            name is inferred from overriden or default data-
                            folder-path and file-name-prefix)
      --delivery-directory DELIVERY_DIRECTORY
                            Delivery directory to put on bucket (if not specified,
                            default value for the vendor will be used)
      --bucket-name BUCKET_NAME
                            Bucket name (if not specified, default value for the
                            vendor will be used; use " " if you don't want to
                            upload the converted file to any bucket)
      --xml-output INTERMEDIATE_XML_OUTPUT_PATH
                            Path to output intermedite xml file (if not provided,
                            it will be deleted)
      --db-server DB_SERVER
                            Database server (if not provided, default val will be
                            used)
      --db-username DB_USERNAME
                            Database username (if not provided, default val will
                            be used)
      --db-password DB_PASSWORD
                            Database password (if not provided, default val will
                            be used)
      --db-name DB_NAME     Database name (if not provided, default val will be
                            used)
      --db-port DB_PORT     Database port (if not provided, default val will be
                            used)
      --db-context [DB_CONTEXT [DB_CONTEXT ...]]
                            Database context variabels in key=val format (if not
                            provided, default values will be used)
      --aws-region AWS_REGION_NAME
                            AWS region name (if not provided, default value from
                            profile will be used)
      --aws-access-key AWS_ACCESS_KEY
                            AWS access key (if not provided, default value from
                            profile will be used)
      --aws-secret-key AWS_SECRET_KEY
                            AWS secret key (if not provided, default value from
                            profile will be used)

#### Usage Examples
`python convert_db_to_834.py --vendor UCH_834_ASMO`

`python convert_db_to_834.py --vendor UCH_834_ASMO --output result.834`

`python convert_db_to_834.py --vendor UCH_834_ASMO --output result.834 --db-username root --db-password xxxxxxx`

`python convert_db_to_834.py --vendor UCH_834_ASMO --db-context companyId=12 carrierId=66`

`python convert_db_to_834.py --vendor UCH_834_ASMO --aws-access-key xxxxxxx -aws-secret-key xxxxxxx`

`python convert_db_to_834.py --vendor UCH_834_ASMO --bucket-name my-test-bucket`

----

### Converting from CSV to 834

#### Usage
`convert_csv_to_834.py csv_file_path --vendor VENDOR [-h]
                             [--data-folder-path DATA_FOLDER_PATH]
                             [--file-name-prefix FILE_NAME_PREFIX]
                             [--delivery-name DELIVERY_NAME] [--is-production]
                             [--output-edi-path OUTPUT_EDI_PATH]
                             [--delivery-directory DELIVERY_DIRECTORY]
                             [--bucket-name BUCKET_NAME]
                             [--xml-output INTERMEDIATE_XML_OUTPUT_PATH]
                             [--csv-delimiter CSV_DELIMITER]
                             [--csv-quote-char CSV_QUOTE_CHAR]
                             [--aws-region AWS_REGION_NAME]
                             [--aws-access-key AWS_ACCESS_KEY]
                             [--aws-secret-key AWS_SECRET_KEY]`

#### Description of command-line arguments
    positional arguments:
      csv_file_path         Path to input CSV file

    mandatory arguments:
      --vendor              {AETNA_MEDICAL_834_FIS,
                             ANTHEM_LIFE_834_FELLFAB,
                             ANTHEM_MULTICOVERAGE_834_ASMO,
                             ANTHEM_MULTICOVERAGE_834_FELLFAB,
                             CIGNA_DENTAL_834_ASMO,
                             CIGNA_MULTICOVERAGE_834_ASMO,
                             CIGNA_MULTICOVERAGE_834_COLTENE,
                             CIGNA_MULTICOVERAGE_834_CW,
                             CIGNA_MULTICOVERAGE_834_HFHI,
                             CIGNA_MULTICOVERAGE_834_NBOI,
                             CIGNA_MULTICOVERAGE_834_PALA,
                             CIGNA_MULTICOVERAGE_834_SPRINGFIELD,
                             DELTADENTAL_MULTICOVERAGE_834_SPRINGFIELD,
                             EMBLEM_834_ARCHCARE,
                             EXPRESSSCRIPTS_834_SPRINGFIELD,
                             UCH_834_ASMO,
                             VSP_VISION_834_ARCHCARE}
                            Receiver-sender combination code, such as UHC_834_ASMO
                            (same as Talend job names)

    optional arguments:
      -h, --help            show this help message and exit
      --data-folder-path DATA_FOLDER_PATH
                            Path to data folder for local files (if not specified,
                            default value for the vendor will be used)
      --file-name-prefix FILE_NAME_PREFIX
                            File name prefix for local files (if not specified,
                            default value for the vendor will be used)
      --delivery-name DELIVERY_NAME
                            Delivery name to put on bucket (if not specified,
                            default value for the vendor will be used)
      --is-production       Flag for production environment (if not specified,
                            default value for the vendor will be used)
      --output-edi-path OUTPUT_EDI_PATH
                            Path to the output x12 834 edi file (if not specified,
                            name is inferred from overriden or default data-
                            folder-path and file-name-prefix)
      --delivery-directory DELIVERY_DIRECTORY
                            Delivery directory to put on bucket (if not specified,
                            default value for the vendor will be used)
      --bucket-name BUCKET_NAME
                            Bucket name (if not specified, default value for the
                            vendor will be used; use " " if you don't want to
                            upload the converted file to any bucket)
      --xml-output INTERMEDIATE_XML_OUTPUT_PATH
                            Path to output intermedite xml file (if not provided,
                            it will be deleted)
      --csv-delimiter CSV_DELIMITER
                            CSV delimiter (defaults to comma)
      --csv-quote-char CSV_QUOTE_CHAR
                            CSV quote character (defaults to null; double quotes
                            are automatically detected)
      --aws-region AWS_REGION_NAME
                            AWS region name (if not provided, default value from
                            profile will be used)
      --aws-access-key AWS_ACCESS_KEY
                            AWS access key (if not provided, default value from
                            profile will be used)
      --aws-secret-key AWS_SECRET_KEY
                            AWS secret key (if not provided, default value from
                            profile will be used)

#### Usage Examples
`python convert_csv_to_834.py __vendors-test-data__\UCH_834_ASMO\UCH_834_ASMO.csv --vendor UCH_834_ASMO`

`python convert_csv_to_834.py __vendors-test-data__\UCH_834_ASMO\UCH_834_ASMO.csv --vendor UCH_834_ASMO --output result.834`

`python convert_csv_to_834.py __vendors-test-data__\UCH_834_ASMO\UCH_834_ASMO.csv --vendor UCH_834_ASMO --output result.834 --csv-delimiter ";"`