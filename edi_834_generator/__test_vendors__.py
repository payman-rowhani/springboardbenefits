import os
import tempfile
import traceback
import argparse
import sys
from datetime import datetime
from pprint import pprint

import core.convert_utils as convert_utils
from convert_csv_to_834 import convert_csv_to_834

# command-line arg parser
def _validate_input():
  vendors = convert_utils.get_vendor_names()
  parser = argparse.ArgumentParser(description='Test 834 Vender Converters.')
  parser.add_argument('--vendor', dest='vendor', required=False, choices=vendors, help=f'Test only a specific vendor seperated by comma')
  parser.add_argument('--only-query', dest='only_query', action='store_true', help='Flag for checking only query match')

  try:
    args = parser.parse_args()
    return args.vendor, args.only_query
  except:
    parser.print_help()
    sys.exit(0)

# if run from console (command-line)
if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  vendor, only_query = _validate_input()
  vendors = [(vendor or '').upper()]
  if not vendors[0]:
   vendors = convert_utils.get_vendor_names()

  content_result = {}
  query_result = {}
  for vendor in vendors:
    print(f'Checking query for {vendor}...')
    target_sql_file_path = f'__vendors-test-data__\\{vendor}\\{vendor}.sql'
    with open(target_sql_file_path, 'rt') as target_stream:
      vendor_cls = convert_utils.get_vendor_class(vendor)
      target_query = target_stream.read().replace(' ', '').replace('\n', '').replace('\r', '').strip().lower()
      test_query = vendor_cls.get_db_query(raw=True).replace(' ', '').replace('\n', '').replace('\r', '').strip().lower()
      query_result[vendor] = "Matched" if target_query == test_query else "Not Matched"
      print(f'Query check result for {vendor}: {query_result[vendor]}')
      if only_query: print()

    csv_file_path = f'__vendors-test-data__\\{vendor}\\{vendor}.csv'
    test_xml_file_path = tempfile.NamedTemporaryFile(mode='w', newline='', encoding='utf-8', suffix='.xml').name

    if only_query:
      continue

    print(f'Converting {vendor} to xml representation...')
    try:
      convert_csv_to_834(
        vendor=vendor,
        csv_file_path=csv_file_path,
        csv_delimiter=';',
        output_edi_path=None,
        intermediate_xml_output_path=test_xml_file_path,
        pretty_intermediate_xml=True,
        keep_intermediate_xml=True,
        now=datetime.utcnow(),
        verbose=False,
        create_xml_only=True
      )
    except:
      content_result[vendor] = 'Error'
      traceback.print_exc()

    if vendor not in content_result:
      print(f'Checking content for {vendor}...')

      target_xml_file_path = f'__vendors-test-data__\\{vendor}\\{vendor}.xml'
      line_counter = 0
      with open(target_xml_file_path, 'rt') as target_stream:
        with open(test_xml_file_path, 'rt') as test_stream:
          for target_line in target_stream:
            line_counter += 1
            test_line = ''
            try:
              test_line = test_stream.readline()
            except:
              content_result[vendor] = 'Failed'
              break
            if line_counter in [13, 14, 27, 28, 43, 44, 55]: # ignore date/time fields on root xml template as they are dynamic
              continue
            if test_line.strip() != target_line.strip():
              content_result[vendor] = 'Failed'
              break
      if vendor not in content_result:
        content_result[vendor] = 'Passed'

    print(f'Content check result for {vendor}: {content_result[vendor]}\n')

  summary = 'All' if all(v == 'Matched' for k, v in query_result.items()) else 'Some' if any(v == 'Matched' for k, v in query_result.items()) else 'None'
  print('-----------------------------')
  print(f'Summary of query check results ({summary} Matched):')
  pprint(query_result)

  if not only_query:
    summary = 'All' if all(v == 'Passed' for k, v in content_result.items()) else 'Some' if any(v == 'Passed' for k, v in content_result.items()) else 'None'
    print('\n-----------------------------')
    print(f'Summary of content check results ({summary} Passed):')
    pprint(content_result)