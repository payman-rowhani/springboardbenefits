import core.convert_utils as convert_utils
from core.csv_reader import CsvReader

# main function for conversion
def convert_csv_to_834(vendor, csv_file_path, csv_delimiter=',', csv_quote_char=None, data_folder_path=None, file_name_prefix=None, delivery_name=None, is_production=False, output_edi_path=None, delivery_directory=None, bucket_name=None, intermediate_xml_output_path=None, pretty_intermediate_xml=False, keep_intermediate_xml=False, now=None, verbose=True, create_xml_only=False, aws_region_name=None, aws_access_key=None, aws_secret_key=None):
  """
    Top-level function to convert a CSV file into x12 834 edi file.

    Args:
      vendor (str): Receiver-sender combination code, such as UHC_834_ASMO (same as Talend job names).
      csv_file_path (str): The path to the input csv file.
      csv_delimiter? (str): The optional csv delimiter (defaults to comma).
      csv_quote_char? (str): The optional csv quote character (defaults to null).
      data_folder_path? (str): The optional path to data folder for local files (if not specified, default value for the vendor will be used).
      file_name_prefix? (str): The optional file name prefix for local files (if not specified, default value for the vendor will be used).
      delivery_name? (str): The optional delivery name to put on bucket (if not specified, default value for the vendor will be used).
      is_production? (bool): The optional flag for production environment (if not specified, default value for the vendor will be used).
      output_edi_path? (str): The optional path to the output x12 834 edi file (if not specified, name is inferred from overriden or default data_folder_path and file_name_prefix).
      delivery_directory? (str): The optional delivery directory to put on bucket (if not specified, default value for the vendor will be used).
      bucket_name? (str): The optional bucket name (if not specified, default value for the vendor will be used; use " " if you don't want to upload the converted file to any bucket).
      intermediate_xml_output_path? (str): The optional path to the intermediate xml-representation of x12 834 edi file (if not passed, a temp file will be generated).
      pretty_intermediate_xml? (bool): The optional flag indicating the intermediate generated xml should be formatted and pretty (usefull for debugging).
      keep_intermediate_xml? (bool): The optional flag indicating the intermediate generated xml should be kept.
      now? (datetime): The optional value to represent current time (if not passed, current utc will be used).
      verbose? (bool): The optional flag to print output verbos.
      create_xml_only? (bool): The optional flag to indicate EDI file need not to be created (useful for testing).
      aws_region_name? (str): The optional AWS region name (if not provided, default value from profile will be used).
      aws_access_key? (str): The optional AWS access key (if not provided, default value from profile will be used).
      aws_secret_key? (str): The optional AWS secret key (if not provided, default value from profile will be used).

    Returns:
      The path to local x12 834 edi file (str).
  """

  if verbose: print(f'Loading data from CSV file for vendor {vendor}...')
  details_data_dict_list = CsvReader(csv_file_path, csv_delimiter, csv_quote_char, now=now).get_data_dict_list()

  return convert_utils.convert_data_list_to_834(
    details_data_dict_list=details_data_dict_list,
    vendor=vendor,
    db_context=None,
    data_folder_path=data_folder_path,
    file_name_prefix=file_name_prefix,
    delivery_name=delivery_name,
    is_production=is_production,
    output_edi_path=output_edi_path,
    delivery_directory=delivery_directory,
    bucket_name=bucket_name,
    intermediate_xml_output_path=intermediate_xml_output_path,
    pretty_intermediate_xml=pretty_intermediate_xml,
    keep_intermediate_xml=keep_intermediate_xml,
    now=now,
    verbose=verbose,
    create_xml_only=create_xml_only,
    aws_region_name=aws_region_name,
    aws_access_key=aws_access_key,
    aws_secret_key=aws_secret_key
  )

# if run from console (command-line)
if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  args = convert_utils.get_command_line_args(converter_type="csv")
  convert_csv_to_834(**args)