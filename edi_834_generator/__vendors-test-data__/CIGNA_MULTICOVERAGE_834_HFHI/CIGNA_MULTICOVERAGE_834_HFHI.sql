DECLARE @ONEWEEKAGO Date = DATEADD(DAY, -{DaysInPast}, GETDATE());
DECLARE @COMPANYID BIGINT = {companyId};
DECLARE @CARRIERID BIGINT = {carrierId};
DECLARE @ENROLLMENTPERIODID BIGINT = {enrollmentPeriodId};
select *,
Case When CancellationDateTime is not null and CancellationDateTime > @ONEWEEKAGO and
CancellationDateTime <= GETDATE()
Then CancellationDateTime
When CancellationDateTime is not null and CancellationDateTime < @ONEWEEKAGO and
EnrollmentModified > @ONEWEEKAGO
Then CancellationDateTime
Else null
End as [Canceled],
case  When CancellationDateTime is not null and CancellationDateTime > @ONEWEEKAGO and
CancellationDateTime <= GETDATE()
Then CancellationDateTime
When CancellationDateTime is not null and CancellationDateTime < @ONEWEEKAGO and
EnrollmentModified > @ONEWEEKAGO
Then CancellationDateTime
When CancellationDateTime is null and Dependent_Cancellation_Date > @ONEWEEKAGO and
Dependent_Cancellation_Date <= GETDATE()
Then Dependent_Cancellation_Date
Else null
End as [DependentCanceled]
FROM [iris-production].[dbo].[MemberElectionView]
OUTER APPLY (
SELECT TOP (1) Location_Id FROM LocationHistories WHERE CompanyUser_id = MemberElectionView.CompanyUserId ORDER BY LocationEffectiveDate DESC
) Loc
where Company_id = @COMPANYID and Carrier_ID = @CARRIERID and EnrollmentPeriod_Id = @ENROLLMENTPERIODID
and (CancellationDateTime is null or (CancellationDateTime is not null and CancellationDateTime >
@ONEWEEKAGO and CancellationDateTime <= GETDATE())
or (CancellationDateTime is not null and CancellationDateTime < @ONEWEEKAGO and EnrollmentModified
> @ONEWEEKAGO)
or (CancellationDateTime is not null and CancellationDateTime >= GETDATE())
)
and (Dependent_Cancellation_Date is null or (Dependent_Cancellation_Date is not null and Dependent_Cancellation_Date >
@ONEWEEKAGO and Dependent_Cancellation_Date <= GETDATE())
or (Dependent_Cancellation_Date is not null and Dependent_Cancellation_Date < @ONEWEEKAGO and EnrollmentModified
> @ONEWEEKAGO)
or (Dependent_Cancellation_Date is not null and Dependent_Cancellation_Date >= GETDATE())
)
and BenefitEffectiveDate is not NULL
AND Benefit_Type IN (1,2,3)
ORDER by Employee_SSN DESC, (CASE WHEN Relationship IS NULL THEN 0 ELSE 1 END), Dependent_SSN DESC, Dependent_Last_Name DESC, Dependent_First_Name DESC, Benefit_Type