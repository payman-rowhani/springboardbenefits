DECLARE @ONEWEEKAGO DATE = DATEADD(DAY, -{DaysInPast}, GETDATE());
DECLARE @COMPANYID BIGINT = {companyId};
DECLARE @CARRIERID BIGINT = {carrierId};
DECLARE @ENROLLMENTPERIODID BIGINT = {enrollmentPeriodId};

SELECT *,
       CASE
         WHEN CancellationDateTime IS NOT NULL
              AND CancellationDateTime > @ONEWEEKAGO
              AND CancellationDateTime <= GETDATE() THEN CancellationDateTime
         WHEN CancellationDateTime IS NOT NULL
              AND CancellationDateTime < @ONEWEEKAGO
              AND EnrollmentModified > @ONEWEEKAGO THEN CancellationDateTime
         ELSE NULL
       END AS [Canceled],
       CASE
         WHEN CancellationDateTime IS NOT NULL
              AND CancellationDateTime > @ONEWEEKAGO
              AND CancellationDateTime <= GETDATE() THEN CancellationDateTime
         WHEN CancellationDateTime IS NOT NULL
              AND CancellationDateTime < @ONEWEEKAGO
              AND EnrollmentModified > @ONEWEEKAGO THEN CancellationDateTime
         WHEN CancellationDateTime IS NULL
              AND Dependent_Cancellation_Date > @ONEWEEKAGO
              AND Dependent_Cancellation_Date <= GETDATE() THEN
         Dependent_Cancellation_Date
         ELSE NULL
       END AS [DependentCanceled]
FROM   [iris-production].[dbo].[MemberElectionView]
       OUTER APPLY (SELECT Count(*) DependentCount
                    FROM   FamilyMemberEnrollments fme
                    WHERE  fme.Enrollment_Id = MemberElectionView.Enrollment_Id
                           AND ( BenefitEndDate IS NULL
                                  OR ( BenefitEndDate IS NOT NULL
                                       AND BenefitEndDate > @ONEWEEKAGO
                                       AND BenefitEndDate <= GETDATE() )
                                  OR ( BenefitEndDate IS NOT NULL
                                       AND BenefitEndDate < @ONEWEEKAGO
                                       AND ModifiedAt > @ONEWEEKAGO )
                                  OR ( BenefitEndDate IS NOT NULL
                                       AND BenefitEndDate >= GETDATE() ) ))
                   DepCount
WHERE  Company_id = @COMPANYID
       AND Carrier_ID = @CARRIERID
       AND EnrollmentPeriod_Id = @ENROLLMENTPERIODID
       AND ( CancellationDateTime IS NULL
              OR ( CancellationDateTime IS NOT NULL
                   AND CancellationDateTime > @ONEWEEKAGO
                   AND CancellationDateTime <= GETDATE() )
              OR ( CancellationDateTime IS NOT NULL
                   AND CancellationDateTime < @ONEWEEKAGO
                   AND EnrollmentModified > @ONEWEEKAGO )
              OR ( CancellationDateTime IS NOT NULL
                   AND CancellationDateTime >= GETDATE() ) )
       AND ( Dependent_Cancellation_Date IS NULL
              OR ( Dependent_Cancellation_Date IS NOT NULL
                   AND Dependent_Cancellation_Date > @ONEWEEKAGO
                   AND Dependent_Cancellation_Date <= GETDATE() )
              OR ( Dependent_Cancellation_Date IS NOT NULL
                   AND Dependent_Cancellation_Date < @ONEWEEKAGO
                   AND EnrollmentModified > @ONEWEEKAGO )
              OR ( Dependent_Cancellation_Date IS NOT NULL
                   AND Dependent_Cancellation_Date >= GETDATE() ) )
       AND BenefitEffectiveDate IS NOT NULL
--ORDER by Employee_Last_Name DESC, (CASE WHEN Relationship IS NULL THEN 0 ELSE 1 END), Dependent_SSN DESC, Dependent_Last_Name DESC, Dependent_First_Name DESC, Benefit_Type